# README #

Multi-threaded simulation of wireless nodes passing information for the purpose of analyzing the efficiency of different routing protocols.


### Project Details ###

* [UML](https://drive.google.com/file/d/0B8FeDq0ctw_2LTBWYWdtSDBNWGs/view?pli=1)
* [Class Descriptions](https://docs.google.com/document/d/1GahAACe_-QmexI8-W4JRn2Vl59Fg1kXMBy0F7_Kx6AU/edit?usp=sharing)
* [Class Stories](https://docs.google.com/document/d/1GahAACe_-QmexI8-W4JRn2Vl59Fg1kXMBy0F7_Kx6AU/edit?usp=sharing)

### Who do I talk to? ###

* casadiochristian@gmail.com
* cmicklis@stetson.edu