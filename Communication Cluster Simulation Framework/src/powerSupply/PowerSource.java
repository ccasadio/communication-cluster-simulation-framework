package powerSupply;

public class PowerSource implements PowerSupply
{
	private int myPower;
	
	public PowerSource(int power)
	{
		myPower = power;
	}
	
	public int usePower(int power) 
	{
		if(power < 0)
		{
			System.err.println(this + "(PowerSource).usePower() - negative power argument.");
			return -1;
		}
		else
		{
			if(power > myPower)
			{
				return -1;
			}
			else
			{
				return myPower - power;
			}
		}
	}
	
	public int gainPower()
	{
		return myPower;
	}
}
