package powerSupply;

public class Battery implements PowerSupply 
{
	private int myCurrentPower,
				myMaxPowerCapacity;
	
	//constructor that assumes the starting power is max
	public Battery(int capacity)
	{
		myCurrentPower = capacity;
		myMaxPowerCapacity = capacity;
	}
	
	//specify starting power and max capacity
	public Battery(int power, int capacity)
	{
		myCurrentPower = power;
		myMaxPowerCapacity = capacity;
	}
	
	public int usePower(int power) 
	{
		if(power < 0)
		{
			System.err.println(this + "(Battery).usePower() - negative power argument.");
			return -1;
		}
		else
		{
			int newCurPow = myCurrentPower - power;
			if(newCurPow > 0)
			{
				myCurrentPower = newCurPow;
				return myCurrentPower;
			}
			else
			{
				return -1;
			}
		}
	}

	public int storePower(int power) 
	{
		if(power < 0)
		{
			System.err.println(this + "(Battery).storePower - negative power argument.");
			return 0;
		}
		else
		{
			int newCurPow = myCurrentPower + power;
			if(newCurPow > myMaxPowerCapacity)
			{
				myCurrentPower = myMaxPowerCapacity;
				return newCurPow - myMaxPowerCapacity;
			}
			else
			{
				return 0;
			}
		}
	}

	public int currentPower() 
	{
		return myCurrentPower;
	}

}
