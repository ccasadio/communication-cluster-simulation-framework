package powerSupply;

public class UnlimitedPowerSource extends PowerSource
{
	public static final int LARGE_NUMBER = 1000000;
	
	public UnlimitedPowerSource() 
	{
		super(LARGE_NUMBER);
	}
	
	public int usePower(int power) 
	{
		if(power < 0)
		{
			System.err.println(this + "(PowerSource).usePower() - negative power argument.");
			return -1;
		}
		else
		{
			return LARGE_NUMBER;
		}
	}
}
