package powerSupply;

import java.util.ArrayList;

public class PowerSupplyController
{
	private ArrayList<PowerSupply> myPowerSources;
	private double t1;
	private double t2;
	private double elapsedTime;
	
	public PowerSupplyController()
	{
		myPowerSources = new ArrayList<PowerSupply>();
		t1 = ((double)System.nanoTime())/1000000000;
	}
	
	public PowerSupplyController(PowerSupply... supplies)
	{
		this();
		
		for(PowerSupply supply : supplies)
		{
			myPowerSources.add(supply);
		}
	}
	
	public void addPowerSupply(PowerSupply ps)
	{
		myPowerSources.add(ps);
	}
	
	public boolean usePower(int power)
	{
		if(currentPower() < power)
		{
			return false;
		}
		else
		{
			for(PowerSupply supply : myPowerSources)
			{
				if(supply.getClass() == Battery.class)
				{
					power = ((Battery) supply).usePower(power);
				}
			}
			for(PowerSupply supply : myPowerSources)
			{
				if(supply.getClass() == PowerSource.class)
				{
					power -= elapsedTime * ((PowerSource) supply).gainPower();
				}
			}
			return true;
		}
	}
	
	public int currentPower()
	{
		t2 = ((double)System.nanoTime())/1000000000;
		elapsedTime = Math.abs(t2 - t1);
		int temp1 = 0;
		int temp2 = 0;
		for(PowerSupply supply : myPowerSources)
		{
			if(supply.getClass() == PowerSource.class)
			{
				temp1 += elapsedTime * ((PowerSource) supply).gainPower();
				temp2 += elapsedTime * ((PowerSource) supply).gainPower();
			}
			else if(supply.getClass() == Battery.class)
			{
				temp1 += ((Battery) supply).currentPower();
			}
			else
			{
				System.err.println(this + "(PowerSource).currentPower() - unspecified PowerSupply type.");
			}
		}
		for(PowerSupply supply : myPowerSources)
		{
			if(supply.getClass() == Battery.class)
			{
				temp2 = ((Battery) supply).storePower(temp2);
			}
		}
		t1 = t2;
		return temp1;
	}
}
