package communication;

import sun.misc.Queue;
import node.*;

public class Communication 
{
	private static int myPacketCounter;
	private static Queue<Parcel> mySendQueue;
	
	public static void constructQueue()
	{
		mySendQueue = new Queue<Parcel>();
		myPacketCounter = 0;
	}
	
	public static void sendMessage(Packet pk, int maxDist, Node sender)
	{
		mySendQueue.enqueue(new Parcel(pk, maxDist, sender));
	}
	
	public static Parcel getParcel()
	{
		try 
		{
			return mySendQueue.dequeue();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public static void deliverMessage(Packet pk, double sigStr, int impedVal, Node recip)
	{
		if(calculateIsDelivered(sigStr, impedVal))
		{
			recip.addToInbox(pk);
		}
	}
	
	private static boolean calculateIsDelivered(double sigStr, int impVal)
	{
		return true;
	}
	
	public static void incrementPacketCounter()
	{
		myPacketCounter ++;
	}
	
	public static int getPacketCounter()
	{
		return myPacketCounter;
	}
}