package communication;

/**
 * The Header class acts as a network header, storing all of the 
 * necessary information that a normal header would  contain. The
 * address of whom the packet is for, the sender of the packet, 
 * when the packet was created, and what ID the packet is to hold.
 * 
 * @author Christian Casadio, Christian Micklisch
 */

import java.util.Calendar;
import java.util.Date;

public class Header 
{
	public static final int TOTAL_BITS = 112;
	
	private Date myTimeStamp;
	private Calendar myCalendar;
	private String myTimeCreated;
	private long myCreatedMillis;
	private int[] mySender;
	private int[] myReceiver;
	private int myMessageLength;
	private int myID;
	
	/**
	 * The Header object is there to store the receiver, sender,
	 * the time of creation, and its unique ID. The unique ID is there
	 * to define the Packet, but all it is is just the counter for the
	 * amount packet already in existence.
	 * 
	 * @param sender
	 * @param receiver
	 */
	public Header(int[] sender, int[] receiver, int messageLength)
	{
		mySender = sender;
		myReceiver = receiver;
		myMessageLength = messageLength;
		myCreatedMillis = System.currentTimeMillis();
		myTimeStamp = new Date();
		myCalendar = Calendar.getInstance();
		myCalendar.setTime(myTimeStamp);	
		// The header needs an ID
		myID = Communication.getPacketCounter();
		Communication.incrementPacketCounter();
		this.setTimeCreated();
	}
	
	public Header(int[] sender, int[] receiver, int messageLength, int id, String timeCreated, long createdMillis)
	{
		mySender = sender;
		myReceiver = receiver;
		myMessageLength = messageLength;
		myID = id;
		myTimeCreated = timeCreated;
		myCreatedMillis = createdMillis;
	}
	
	public int getID()
	{
		return myID;
	}
	
	public int[] getSender()
	{
		return mySender;
	}
	
	public int[] getReceiver()
	{
		return myReceiver;
	}
	
	public int getMessageLength()
	{
		return myMessageLength;
	}
	
	public String getTimeCreated()
	{
		return myTimeCreated;
	}
	
	public long getCreatedMillis()
	{
		return myCreatedMillis;
	}
			
	/**
	 * The setTimeCreated method goes and gets all of the information from the 
	 * Calendar object and then converts it all into a string of format Y-M-D-H-M-S-MS.
	 * It then saves this into the TimeCreated string.
	 * 
	 * @return
	 */
	private void setTimeCreated()
	{
		myTimeCreated = myCalendar.get(Calendar.YEAR) + "-" + myCalendar.get(Calendar.MONTH) + 
				"-" + myCalendar.get(Calendar.DAY_OF_MONTH) + "-" + myCalendar.get(Calendar.HOUR) + 
				"-" + myCalendar.get(Calendar.MINUTE) + "-" + myCalendar.get(Calendar.SECOND) + 
				"-" + myCalendar.get(Calendar.MILLISECOND);
	}
	
	public Header clone()
	{
		int[] sender = mySender.clone();
		int[] receiver = myReceiver.clone();
		int messageLength = myMessageLength;
		int id = myID;
		String timeCreated = myTimeCreated + "";
		return new Header(sender, receiver, messageLength, id, timeCreated, myCreatedMillis);
	}
}
