package communication;

import java.io.UnsupportedEncodingException;

/**
 * The Packet Class is there to act as a packet being sent
 * from one point to another, containing information about
 * the packet, such as the content (message) of the packet,
 * and the header, which includes the uniqueness of the packet. 
 * 
 * @author Christian Casadio, Christian Micklisch
 */

public class Packet 
{
	private Header myHeader;
	private String myMessage;
	private double mySignalStrength;
	
	public Packet(String message, Header header)
	{
		myMessage = message;
		myHeader = header;
		mySignalStrength = 0.0;
	}
	
	public Packet(String message, int[] sender, int[] receiver)
	{
		myHeader = new Header(sender, receiver, message.length());
		myMessage = message;
		mySignalStrength = 0.0;
	}
	
	public Header getHeader()
	{
		return myHeader;
	}
	
	public String getMessage()
	{
		return myMessage;
	}
	
	public double getSignalStrength()
	{
		return mySignalStrength;
	}
	
	public void setMessage(String message)
	{
		myMessage = message;
	}
	
	public void setSignalStrength(double signalStrength)
	{
		mySignalStrength = signalStrength;
	}
	
	/**
	 * The calculateTotalBit method goes and calculates all of the bits that a packet
	 * will have by getting all of the bits in the message and adding it to all of the
	 * bits in the header (which is a constant).
	 * 
	 * @return
	 */
	public int calculateTotalBit()
	{
		byte[] byteArray = null;
		try 
		{
			byteArray = myMessage.getBytes("US-ASCII");
		} 
		catch (UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
		
		int messagebits = byteArray.length * 8;
		
		return messagebits + Header.TOTAL_BITS;
	}
	
	public Packet clone()
	{
		String message = myMessage + "";
		return new Packet(message, myHeader.clone());
	}
}
