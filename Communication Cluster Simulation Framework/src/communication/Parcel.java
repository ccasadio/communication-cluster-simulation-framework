package communication;

import node.Node;

public class Parcel 
{
	private Packet myPacket;
	private int myMaxDistance;
	private Node myNode;
	
	public Parcel(Packet packet, int maxDist, Node node)
	{
		myPacket = packet;
		myMaxDistance = maxDist;
		myNode = node;
	}
	
	public void setNode(Node node)
	{
		myNode = node;
	}
	
	public Packet getPacket()
	{
		return myPacket;
	}
	
	public int getMaxDistance()
	{
		return myMaxDistance;
	}
	
	public Node getNode()
	{
		return myNode;
	}
	
	public Parcel clone()
	{
		return new Parcel(myPacket.clone(), myMaxDistance, myNode.clone());
	}
}
