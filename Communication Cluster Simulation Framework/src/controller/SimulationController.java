package controller;

/**
 * The Controller class is there to act as a communicator between the
 * System (model) and the View. The controller also saves the conditions
 * for initializing the system each time the System is setup. 
 * 
 * The flow of the controller is that the User gets a View displayed to 
 * them in which the controller sends a list of all of the parameters that
 * were used to initialize the System. The user then chooses either to
 * initialize a previous condition or to create a new one. This is then
 * saved again into the system.
 * 
 * @author Christian Casadio, Christian Micklisch, Katie Porterfield
 */

import java.awt.Component;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import environment.ColorMap;
import environment.NodePosition;
import model.Model;
import view.View;

public class SimulationController implements Runnable
{
	public static final String BLANK = "";
	public static final long MAX_RUNTIME = 100000;
	
	private Model myModel;
	private View myView;
	private SavedSystemConditions mySavedSystemConditions;
	private Thread myThread;
	private NodePosition mySelectedNode;
	private boolean myShouldRun;
	private boolean myPauseRun;
	
	public static void main(String[] args)
	{
		SimulationController myController = new SimulationController();
	}
	
	public SimulationController()
	{
		myShouldRun = true;
		myPauseRun = false;
		myModel = new Model();
		myView = new View(this);
		mySavedSystemConditions = new SavedSystemConditions();
		mySelectedNode = null;
	}	
	
	public void createEnvironment()
	{
		myView.hideSidePanels();
		myView.displayCreateEnvironmentPanel();
	}
	
	public void loadEnvironment()
	{
		myView.hideSidePanels();
		myView.displayAllSystemConditions(mySavedSystemConditions.getAllSavedSystemConditions());
	}
	
	public void createNode()
	{
		myView.hideSidePanels();
		myView.displayCreateNodePanel();
	}
	
	public void removeNode()
	{
		myView.hideSidePanels();
		myView.displayAllRemoveNode(myModel.getEnvironment().getNodeList());
	}
	
	public void viewNode()
	{
		myView.hideSidePanels();
		myView.displayAllViewNode(myModel.getEnvironment().getNodeList());
	}
	
	public void pauseNode()
	{
		mySelectedNode.getNode().pause();
		myView.disableNodePause();
	}
	
	public void unpauseNode()
	{
		mySelectedNode.getNode().unpause();
		myView.enableNodePause();
	}
	
	/**
	 * The saveCondition function gets the condition that was sent in
	 * by the view in all of the text fields. This information is then sent 
	 * to the System object to setup the System object properly. Once this has
	 * been done the method then gets all of the necessary parameters for the 
	 * conditions, and sends in the needed parameters to the SavedSystemConditions
	 * object to then save the initialized System to then be reloaded again.
	 */
	public void saveCondition()
	{
		if (!myView.getCreateEnvironmentNameTextField().equals(BLANK)
				|| !myView.getCreateEnvironmentNodeCountTextField().equals(BLANK)
				|| !myView.getCreateEnvironmentHubCountTextField().equals(BLANK)
				|| !myView.getCreateEnvironmentWidthTextField().equals(BLANK)
				|| !myView.getCreateEnvironmentHeightTextField().equals(BLANK))
		{
			// currently a test
			String name = myView.getCreateEnvironmentNameTextField();
			String messageType = "messageType";
			String dataType = "dataType";
			int messagesToReceive = 200;
			int nodeCount = Integer.parseInt(myView.getCreateEnvironmentNodeCountTextField());
			int hubCount = Integer.parseInt(myView.getCreateEnvironmentHubCountTextField());
			int width = Integer.parseInt(myView.getCreateEnvironmentWidthTextField());
			int height = Integer.parseInt(myView.getCreateEnvironmentHeightTextField());
			
			myModel.setupModel(nodeCount, hubCount, width, height, messageType, dataType, messagesToReceive, name);
	
			mySavedSystemConditions.saveSystemCondition(name, myModel.getEnvironment().getColorMap(), 
							messageType, dataType, nodeCount);
			myView.setEnvironmentImage(myModel.getEnvironment().getColorMap().getColorMapInBufferedImage());
			myView.hideSidePanels();
			myView.displayStart();
			myView.displayNodeMenu();
		}
		else
		{
			JOptionPane.showMessageDialog(null, "Woaw Man! You forgot to input some value!");
		}
	}

	/**
	 * The loadCondition method goes and gets the name and timestamp of the selected
	 * condition displayed to the user. It then saves the selection to the System
	 * Conditions file and loads up and initializes the proper condition selected for
	 * the System object.
	 */
	public void loadCondition()
	{
		int index = myView.getSelectedCondition();
		ArrayList list = mySavedSystemConditions.getSystemCondition(index);
		myModel.setupModel((ColorMap) list.get(1), (String) list.get(2), (String) list.get(3), (Integer) Integer.parseInt((String)list.get(4)), (String) list.get(0));
		myView.setEnvironmentImage(myModel.getEnvironment().getColorMap().getColorMapInBufferedImage());
		myView.hideSidePanels();
		myView.displayStart();
		myView.displayNodeMenu();
	}
	
	/**
	 * The createNodeCondition method goes and creates a node from the values given by the user
	 * in the view. It then saves the conditions of the environment, as a change was made, and then
	 * updates the image of the environment in the view. 
	 */
	public void createNodeCondition()
	{
		myView.hideSidePanels();
		myModel.createNode(myView.getCreateNodeXTextField(), myView.getCreateNodeYTextField(), myView.getCreateNodeHubCheckBox());

		mySavedSystemConditions.saveSystemCondition(myModel.getName(), myModel.getEnvironment().getColorMap(), 
						myModel.getNodeController().getMessagePassingType(), myModel.getNodeController().getDataGeneratorType(), myModel.getEnvironment().getNodeList().size());
		myView.setEnvironmentImage(myModel.getEnvironment().getColorMap().getColorMapInBufferedImage());
		myView.hideSidePanels();
	}
	
	/**
	 * The removeNodeCondition method goes and removes a node from the index given by the user
	 * in the view. It then saves the conditions of the environment, as a change was made, and then
	 * updates the image of the environment in the view. 
	 */
	public void removeNodeCondition()
	{
		myView.hideSidePanels();
		myModel.removeNode(myView.getSelectedRemoveNodeCondition());
		mySavedSystemConditions.saveSystemCondition(myModel.getName(), myModel.getEnvironment().getColorMap(), 
						myModel.getNodeController().getMessagePassingType(), myModel.getNodeController().getDataGeneratorType(), myModel.getEnvironment().getNodeList().size());
		myView.setEnvironmentImage(myModel.getEnvironment().getColorMap().getColorMapInBufferedImage());
		myView.hideSidePanels();
	}
	
	public void viewNodeCondition()
	{
		myView.hideSidePanels();
		mySelectedNode = myModel.getEnvironment().getNodeList().get(myView.getSelectedViewNodeCondition());
		myView.displayNodeInformationPanel();
		myView.updateNodeInformation(mySelectedNode);
	}
	
	public void start()
	{
		myThread = new Thread(this);
		myThread.start();
		myView.hideStart();
	}
	
	public void pause(int time)
	{
		try 
		{
			Thread.sleep(time);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		myPauseRun = true;
		myModel.pauseNodes();
		myView.hidePause();
		myView.displayUnpause();
	}
	
	public void unpause()
	{
		myPauseRun = false;
		myModel.unpauseNodes();
		myView.hideUnpause();
		myView.displayPause();
	}
	
	public void stop()
	{
		myShouldRun = false;
		myModel.stopNodes();
	}

	/**
	 * The run method goes and starts the model, updates the view if necessary
	 * and then pauses for an alloted amount of time. This is done until it was
	 * either told to stop or it went over its MAX_RUNTIME.
	 */
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		myModel.start();
		while(myShouldRun)
		{
			if (!myPauseRun)
			{
				if (mySelectedNode != null)
				{
					myView.updateNodeInformation(mySelectedNode);
				}
			}
			pause(30);
		}
		System.out.println("stop");
		this.stop();
	}
	
	/**
	 * The view node method goes and displays all of the information regarding
	 * the node that was clicked, this means that we have to get the value of
	 * the node from the coordinate system of the view to the coordinate system
	 * of the environment. 
	 *  
	 * @param x
	 * @param y
	 */
	public void viewNode(int x, int y) 
	{
		double ratio = (double) myModel.getEnvironment().getColorMap().getWidth() / (double) myView.HEIGHT;
		NodePosition n = myModel.getEnvironment().getNodePosition((int) Math.floor(x * ratio), (int) Math.floor(y * ratio));
		if (n != null)
		{
//			System.out.println("here" + n.getNode());
			mySelectedNode = n;	
			myView.displayNodeInformationPanel();
			myView.updateNodeInformation(mySelectedNode);
			
			if (n.getNode().getPaused())
			{
				myView.disableNodePause();
			}
			else
			{
				myView.enableNodePause();
			}
		}
	}
}
