package controller;


/**
 * The SavedSystemConditions class is there to save the system
 * initialization parameters that the user chooses and make that
 * saved information unique by adding a name and a timestamp with
 * it, this then allows the user to go back and load previous 
 * initializations to retest a previous setups. This allows
 * users to control their system at a greater rate.
 * 
 * @author Christian Casadio, Christian Micklisch
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.imageio.ImageIO;

import environment.ColorMap;

public class SavedSystemConditions
{
	public static final String FILE_NAME = "src/controller/SystemConditions.txt";
	public static final char DELIMETER = ';';
	public static final char NEW_LINE = '\n';
	public static final int MINIMUM_CONDITIONS = -1;
	
	
	private BufferedReader myBufferedReader;
	private BufferedWriter myBufferedWriter;
	private FileReader myFileReader;
	private FileWriter myFileWriter;
	private File myFile;
	private Date myTimeStamp;
	private Calendar myCalendar;
	
	public SavedSystemConditions()
	{ 
		myFile = new File(FILE_NAME);
		myTimeStamp = new Date();
		myCalendar = Calendar.getInstance();
		myCalendar.setTime(myTimeStamp);		
	}
	
	/**
	 * The method gets all of the file contents from the myFile object.
	 * It stores all of the values in an arraylist consisting of
	 * arraylists. The arraylist inside of the arraylist contains the
	 * name of the file, the ColorMap object, the message passing type,
	 * the data generator type, and the timestamp of when it was stored 
	 * respectively. Since the second value of the read in value is to 
	 * be a ColorMap object the value stored in the file is that of the
	 * filename of an image, this image file is read, and converted to a
	 * ColorMap and the object is then stored in the array list. The method
	 * then returns an arrayList of all this information. 
	 * 
	 * @return
	 */
	public ArrayList<ArrayList> getAllSavedSystemConditions()
	{
		ArrayList<ArrayList> list = new ArrayList<ArrayList>();
		
		try
		{
			myFileReader = new FileReader(myFile);
			myBufferedReader = new BufferedReader(myFileReader);

			int input = 0;
			int i = 0;
			int j = 0;
			String line = new String();
			list.add(new ArrayList());
			while(input != -1)
			{
				if((char) input == NEW_LINE)
				{
					list.get(j).add(line);
					i = 0;
					j ++;
					line = "";
					list.add(new ArrayList());
				}
				else if((char) input == DELIMETER)
				{
					if(i != 1)
					{
						list.get(j).add(line);
					}
					else
					{
						list.get(j).add(this.convertImageToColorMap(line));
					}
					i ++;
					line = "";
				}
				else
				{
					line += (char) input;
				}
				input = myBufferedReader.read();
			}
			
			myBufferedReader.close();
			myFileReader.close();
		}		
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
	/**
	 * The getSystemCondition method goes and gets returns the array of system conditions
	 * from the specified index in the parameter.
	 * 
	 * @param index
	 * @return
	 */
	public ArrayList getSystemCondition(int index)
	{
		ArrayList<ArrayList> list = this.getAllSavedSystemConditions();
		
		if (index < list.size() && index > MINIMUM_CONDITIONS)
		{
			return list.get(index);
		}
		
		return null;
	}
	
	/**
	 * The saveSystemCondition method saves the initialization of the System
	 * to the SystemConditions file. This initialization consists of the name
	 * that the user gives it, the ColorMap that was created, the message type
	 * that the user chose, and the data generator type (if its randomly generated
	 * or off of environment instances). The method simply appends this as a new
	 * line to the myFile object containing the name, the file name and location of the
	 * image from the color map, the message passing type, the data generator, and
	 * a timestamp included. Each value is seperated by the specified delimeter.
	 * 
	 * Currently a mock save
	 * 
	 * @param name			
	 * @param objects
	 * @param messagePassingType
	 * @param dataGeneratorType
	 */
	public void saveSystemCondition(String name, ColorMap colorMap, String messagePassingType, String dataGeneratorType,
							int amountOfMessagesToSend)
	{
		try
		{
			myFileWriter = new FileWriter(myFile, true);
			myBufferedWriter = new BufferedWriter(myFileWriter);
			
			String dateString = this.getTimeStampString();
			
			System.out.println(dateString);
			String imageFileName = "src/images/" + dateString + ".png";
			
			this.saveColorMapAsImageFile(colorMap, imageFileName);
			String input = name + DELIMETER + imageFileName + DELIMETER + 
			  			   messagePassingType + DELIMETER + dataGeneratorType + 
			  			   DELIMETER + amountOfMessagesToSend + DELIMETER + dateString + 
			  			   NEW_LINE;
			
			myBufferedWriter.write(input);
			
			myBufferedWriter.close();
			myFileWriter.close();
		}		
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * The convertImageToColorMap method goes and gets the entire value of a file and
	 * specified by the parameter and constructs a BufferedImage from it to then convert
	 * all of the values of that BufferedImage object to a simple RGB value. This RGB
	 * value is then added as an ID to the ColorMap object, and so it is stored in its
	 * respective X and Y location in the matrix of the ColorMap. It then returns a ColorMap
	 * object.
	 * 
	 * @param fileName
	 * @return
	 */
	public ColorMap convertImageToColorMap(String fileName)
	{
		BufferedImage img = null;
		try 
		{
		    img = ImageIO.read(new File(fileName));
		} 
		catch (IOException e) 
		{
			
		}
		
		int width = img.getWidth();
		int height = img.getHeight();
		ColorMap cm = new ColorMap(width, height);
		
		for(int i = 0; i < width; i ++)
		{
			for(int j = 0; j < height; j ++)
			{
		        int pixel = img.getRGB(j, i);
				int[] id =  this.printPixelARGB(pixel);
				cm.addObject(i, j, id);
			}
		}
		
		return cm;
	}
	
	public int[] printPixelARGB(int pixel) 
	{
		int alpha = (pixel >> 24) & 0xff;
		int red = (pixel >> 16) & 0xff;
		int green = (pixel >> 8) & 0xff;
		int blue = (pixel) & 0xff;
		int[] id = {red, green, blue, alpha};
		return id;
	}
	
	/**
	 * The saveColorMapAsImageFile saves the ColorMap object as a BufferedImage
	 * and then saves it under the specified filename using the ImageIO method
	 * write. It saves the image as a jpg image type.
	 * 
	 * @param cm
	 * @param fileName
	 * @return
	 */
	public boolean saveColorMapAsImageFile(ColorMap cm, String fileName)
	{
		BufferedImage img = cm.getColorMapInBufferedImage();
        File file = new File(fileName);
        try 
        {
			return ImageIO.write(img, "png", file);
		} 
        catch (IOException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public Date getTimeStamp()
	{
		return myTimeStamp;
	}
	
	/**
	 * The getTimeStampString method goes and gets all of the information from the 
	 * Calendar object and then converts it all into a string of format Y-M-D-H-M-S-MS
	 * 
	 * @return
	 */
	public String getTimeStampString()
	{
		String dateString = myCalendar.get(Calendar.YEAR) + "-" + myCalendar.get(Calendar.MONTH) + 
				"-" + myCalendar.get(Calendar.DAY_OF_MONTH) + "-" + myCalendar.get(Calendar.HOUR) + 
				"-" + myCalendar.get(Calendar.MINUTE) + "-" + myCalendar.get(Calendar.SECOND) + 
				"-" + myCalendar.get(Calendar.MILLISECOND);
		
		return dateString;
	}

}
