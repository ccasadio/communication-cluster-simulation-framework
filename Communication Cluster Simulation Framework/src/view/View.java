package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.TextArea;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import communication.Packet;
import node.Neighbor;
import node.Node;
import controller.SimulationController;
import environment.NodePosition;

public class View extends JFrame
{
	public static final int WIDTH = 800;
	public static final int HEIGHT = 500;
	
	private SimulationController myController;
	private ButtonListener myCreateEnvironmentButtonListener,
						   mySaveEnvironmentConditionButtonListener,
						   myLoadEnvironmentButtonListener,
						   myLoadEnvironmentConditionButtonListener,
						   myCreateNodeButtonListener,
						   myCreateNodeConditionButtonListener,
						   myViewNodeButtonListener,
						   myViewNodeConditionButtonListener,
						   myRemoveNodeButtonListener,
						   myRemoveNodeConditionButtonListener,
						   myStartButtonListener,
						   myPauseButtonListener,
						   myPauseNodeButtonListener,
						   myUnpauseButtonListener,
						   myUnpauseNodeButtonListener,
						   myStopButtonListener;
	private JPanel myMainPanel,
				   myEnvironmentPanel,
				   mySidePanel,
				   myNodeInformationPanel,
				   myNodeInformationTextAreaPanel,
				   myNodeAddedInformationPanel,
				   myCreateEnvironmentPanel,
				   myLoadEnvironmentPanel,
				   myCreateNodePanel,
				   myRemoveNodePanel,
				   myViewNodePanel;
	private EnvironmentView myEnvironmentView;
	private JScrollPane mySavedEnvironmentConditionsPanel,
						myRemoveNodeConditionsPanel,
						myViewNodeConditionsPanel;
	private JTextField myCreateEnvironmentNameTextField,
					   myCreateEnvironmentNodeCountTextField,
					   myCreateEnvironmentHubCountTextField,
					   myCreateEnvironmentWidthTextField,
					   myCreateEnvironmentHeightTextField,
					   myCreateNodeXTextField,
					   myCreateNodeYTextField;
	private TextArea myNodeMessagesTextArea,
					 myNodeNeighborsTextArea;
	private JCheckBox myCreateNodeHubCheckBox;
	private JList mySavedEnvironmentSettingsJList,
				  myRemoveNodeJList,
				  myViewNodeJList;
	private DefaultListModel mySavedEnvironmentSettingsDefaultList,
						     myRemoveNodeDefaultList,
						     myViewNodeDefaultList;
	private JLabel myNodeIDLabel,
				   myNodeNeighborLabel,
				   myNodeTotalMessagesReceivedLabel,
				   myNodeCurrentReceiverLabel,
				   myNodeStepsFromHub;
	private JButton mySaveEnvironmentConditionButton,
					myLoadEnvironmentConditionButton,
					myCreateNodeConditionButton,
					myRemoveNodeConditionButton,
					myViewNodeConditionButton,
					myPauseNodeButton,
					myUnpauseNodeButton;
	private JMenuBar myMenuBar;
	private JMenu myEnvironmentMenu,
				  myNodeMenu;
	private JMenuItem myLoadEnvironmentMenuItem,
					  myCreateEnvironmentMenuItem,
					  myCreateNodeMenuItem,
					  myRemoveNodeMenuItem,
					  myViewNodeMenuItem,
					  myStartMenuItem,
					  myPauseMenuItem,
					  myUnpauseMenuItem,
					  myStopMenuItem;
	
	public View(SimulationController controller)
	{
		myController = controller;

		this.associateListeners();

		this.createMenu();
		this.createViewComponents();
		this.hideSidePanels();

		this.add(this.myMainPanel);

		this.setupView();
	}
	
	public void setupView()
	{
		this.setTitle("Communication Cluster Simulation");
		this.setSize(View.WIDTH, View.HEIGHT + 50);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.hidePause();
		this.hideUnpause();
		this.myStartMenuItem.setVisible(false);
		this.myStopMenuItem.setVisible(false);
		this.hideNodeMenu();
	}
	
	private void createViewComponents()
	{
		myMainPanel = new JPanel();
		myMainPanel.setLayout(new BorderLayout());
		
		myEnvironmentPanel = new JPanel();
		mySidePanel = new JPanel();
		myCreateEnvironmentPanel = new JPanel();
		myLoadEnvironmentPanel = new JPanel();
		myNodeInformationPanel = new JPanel();
		myNodeAddedInformationPanel = new JPanel();
		myNodeInformationTextAreaPanel = new JPanel();
		myCreateNodePanel = new JPanel();
		myRemoveNodePanel = new JPanel();
		myViewNodePanel = new JPanel();
		
		myCreateEnvironmentNameTextField = new JTextField();
		myCreateEnvironmentNodeCountTextField = new JTextField();
		myCreateEnvironmentHubCountTextField = new JTextField();
		myCreateEnvironmentWidthTextField = new JTextField();
		myCreateEnvironmentHeightTextField = new JTextField();
		myCreateNodeXTextField = new JTextField();
		myCreateNodeYTextField = new JTextField();
		
		myEnvironmentView = new EnvironmentView(HEIGHT, HEIGHT);
		
		myNodeIDLabel = new JLabel();
		myNodeTotalMessagesReceivedLabel = new JLabel();
		myNodeNeighborLabel = new JLabel();
		myNodeCurrentReceiverLabel = new JLabel();
		myNodeStepsFromHub = new JLabel();
		
		mySavedEnvironmentConditionsPanel = new JScrollPane();
		myRemoveNodeConditionsPanel = new JScrollPane();
		myViewNodeConditionsPanel = new JScrollPane();
		
		myNodeMessagesTextArea = new TextArea();
		myNodeNeighborsTextArea = new TextArea();
		
		myCreateNodeHubCheckBox = new JCheckBox();
		
		mySavedEnvironmentSettingsDefaultList = new DefaultListModel();
		mySavedEnvironmentSettingsJList = new JList(this.mySavedEnvironmentSettingsDefaultList);
		mySavedEnvironmentSettingsJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		mySavedEnvironmentSettingsJList.setLayoutOrientation(JList.VERTICAL);
		mySavedEnvironmentSettingsJList.setVisibleRowCount(-1);
		
		myRemoveNodeDefaultList = new DefaultListModel();
		myRemoveNodeJList = new JList(this.myRemoveNodeDefaultList);
		myRemoveNodeJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		myRemoveNodeJList.setLayoutOrientation(JList.VERTICAL);
		myRemoveNodeJList.setVisibleRowCount(-1);
		
		myViewNodeDefaultList = new DefaultListModel();
		myViewNodeJList = new JList(this.myViewNodeDefaultList);
		myViewNodeJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		myViewNodeJList.setLayoutOrientation(JList.VERTICAL);
		myViewNodeJList.setVisibleRowCount(-1);
		
		mySaveEnvironmentConditionButton = new JButton("Create");
		mySaveEnvironmentConditionButton.addMouseListener(mySaveEnvironmentConditionButtonListener);
		myLoadEnvironmentConditionButton = new JButton("Load");
		myLoadEnvironmentConditionButton.addMouseListener(myLoadEnvironmentConditionButtonListener);
		myCreateNodeConditionButton =  new JButton("Create");
		myCreateNodeConditionButton.addMouseListener(myCreateNodeConditionButtonListener);
		myRemoveNodeConditionButton = new JButton("Remove");
		myRemoveNodeConditionButton.addMouseListener(myRemoveNodeConditionButtonListener);
		myViewNodeConditionButton = new JButton("View");
		myViewNodeConditionButton.addMouseListener(myViewNodeConditionButtonListener);
		myPauseNodeButton = new JButton("Pause");
		myPauseNodeButton.addMouseListener(myPauseNodeButtonListener);
		myUnpauseNodeButton = new JButton("Unpause");
		myUnpauseNodeButton.addMouseListener(myUnpauseNodeButtonListener);
		
		myCreateEnvironmentPanel.setLayout(new GridLayout(7,2));
		myCreateEnvironmentPanel.add(new JLabel("New Env.",SwingConstants.RIGHT));
		myCreateEnvironmentPanel.add(new JLabel(""));
		myCreateEnvironmentPanel.add(new JLabel("Name",SwingConstants.CENTER));
		myCreateEnvironmentPanel.add(myCreateEnvironmentNameTextField);
		myCreateEnvironmentPanel.add(new JLabel("Node Count",SwingConstants.CENTER));
		myCreateEnvironmentPanel.add(myCreateEnvironmentNodeCountTextField);
		myCreateEnvironmentPanel.add(new JLabel("Hub Count",SwingConstants.CENTER));
		myCreateEnvironmentPanel.add(myCreateEnvironmentHubCountTextField);
		myCreateEnvironmentPanel.add(new JLabel("Width",SwingConstants.CENTER));
		myCreateEnvironmentPanel.add(myCreateEnvironmentWidthTextField);
		myCreateEnvironmentPanel.add(new JLabel("Height",SwingConstants.CENTER));
		myCreateEnvironmentPanel.add(myCreateEnvironmentHeightTextField);
		myCreateEnvironmentPanel.add(new JLabel(""));
		myCreateEnvironmentPanel.add(mySaveEnvironmentConditionButton);
		
		myNodeAddedInformationPanel.setLayout(new GridLayout(5,2));
		myNodeAddedInformationPanel.add(new JLabel("My Neighbors: "));
		myNodeAddedInformationPanel.add(myNodeNeighborLabel);
		myNodeAddedInformationPanel.add(new JLabel("Total Messages Received"));
		myNodeAddedInformationPanel.add(myNodeTotalMessagesReceivedLabel);
		myNodeAddedInformationPanel.add(new JLabel("Current Receiver"));
		myNodeAddedInformationPanel.add(myNodeCurrentReceiverLabel);
		myNodeAddedInformationPanel.add(new JLabel("Steps From Hub"));
		myNodeAddedInformationPanel.add(myNodeStepsFromHub);
		myNodeAddedInformationPanel.add(myPauseNodeButton);
		myNodeAddedInformationPanel.add(myUnpauseNodeButton);
		
		
		myCreateNodePanel.setLayout(new GridLayout(5,2));
		myCreateNodePanel.add(new JLabel("Create Node"));
		myCreateNodePanel.add(new JLabel(""));
		myCreateNodePanel.add(new JLabel("X Position: "));
		myCreateNodePanel.add(myCreateNodeXTextField);
		myCreateNodePanel.add(new JLabel("Y Position: "));
		myCreateNodePanel.add(myCreateNodeYTextField);
		myCreateNodePanel.add(new JLabel("Hub: "));
		myCreateNodePanel.add(myCreateNodeHubCheckBox);
		myCreateNodePanel.add(new JLabel());
		myCreateNodePanel.add(myCreateNodeConditionButton);
		
		
		// load environment
		mySavedEnvironmentConditionsPanel.setViewportView(mySavedEnvironmentSettingsJList);
		
		myLoadEnvironmentPanel.setLayout(new BorderLayout());
		myLoadEnvironmentPanel.add(mySavedEnvironmentConditionsPanel, BorderLayout.NORTH);
		myLoadEnvironmentPanel.add(myLoadEnvironmentConditionButton, BorderLayout.SOUTH);
		
		myRemoveNodeConditionsPanel.setViewportView(myRemoveNodeJList);
		
		myRemoveNodePanel.setLayout(new BorderLayout());
		myRemoveNodePanel.add(myRemoveNodeConditionsPanel, BorderLayout.NORTH);
		myRemoveNodePanel.add(myRemoveNodeConditionButton, BorderLayout.SOUTH);
		
		myViewNodeConditionsPanel.setViewportView(myViewNodeJList);
		
		myViewNodePanel.setLayout(new BorderLayout());
		myViewNodePanel.add(myViewNodeConditionsPanel, BorderLayout.NORTH);
		myViewNodePanel.add(myViewNodeConditionButton, BorderLayout.SOUTH);
		
		myNodeInformationPanel.setLayout(new BorderLayout());
		myNodeInformationPanel.add(myNodeIDLabel, BorderLayout.NORTH);
		myNodeInformationPanel.add(myNodeAddedInformationPanel, BorderLayout.CENTER);
		myNodeInformationTextAreaPanel.setLayout(new BorderLayout());
		myNodeInformationTextAreaPanel.add(myNodeMessagesTextArea, BorderLayout.NORTH);
		myNodeInformationTextAreaPanel.add(myNodeNeighborsTextArea, BorderLayout.SOUTH);
		myNodeInformationPanel.add(myNodeInformationTextAreaPanel, BorderLayout.SOUTH);
		
		myEnvironmentPanel.add(myEnvironmentView);

		mySavedEnvironmentConditionsPanel.setPreferredSize(new Dimension(290, 400));
		myRemoveNodeConditionsPanel.setPreferredSize(new Dimension(290, 400));
		myViewNodeConditionsPanel.setPreferredSize(new Dimension(290, 400));
		myCreateEnvironmentPanel.setPreferredSize(new Dimension(280, 400));
		myEnvironmentPanel.setPreferredSize(new Dimension(HEIGHT, HEIGHT));
		mySidePanel.setPreferredSize(new Dimension(300, HEIGHT));
		myNodeInformationPanel.setPreferredSize(new Dimension(300, 500));
		
		mySidePanel.setBorder(BorderFactory.createLineBorder(Color.black)); 
		
		mySidePanel.add(myCreateEnvironmentPanel);
		mySidePanel.add(myLoadEnvironmentPanel);
		mySidePanel.add(myNodeInformationPanel);
		mySidePanel.add(myCreateNodePanel);
		mySidePanel.add(myRemoveNodePanel);
		mySidePanel.add(myViewNodePanel);
		
		myMainPanel.add(myEnvironmentPanel, BorderLayout.WEST);
		myMainPanel.add(mySidePanel, BorderLayout.EAST);
		
		// adding functionality with the view end
		myEnvironmentView.addMouseListener(new MouseListener() {
	        
	        public void mouseClicked(MouseEvent e) {
	        	myController.viewNode(e.getX(), e.getY());
	        }

			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
	    });
	}
	
	public void hideSidePanels()
	{
		myCreateEnvironmentPanel.setVisible(false);
		myNodeInformationPanel.setVisible(false);
		myLoadEnvironmentPanel.setVisible(false);
		myCreateNodePanel.setVisible(false);
		myRemoveNodePanel.setVisible(false);
		myViewNodePanel.setVisible(false);
	}
	
	private void createMenu()
	{
		myMenuBar = new JMenuBar();
		this.setJMenuBar(myMenuBar);
		
		myEnvironmentMenu = new JMenu("Environment");
		myNodeMenu = new JMenu("Node");
		
		myLoadEnvironmentMenuItem = new JMenuItem("Load");
		myCreateEnvironmentMenuItem = new JMenuItem("Create");
		
		myCreateNodeMenuItem = new JMenuItem("Create");
		myRemoveNodeMenuItem = new JMenuItem("Remove");
		myViewNodeMenuItem = new JMenuItem("View");
		
		myStartMenuItem = new JMenuItem("Start");
		myPauseMenuItem = new JMenuItem("Pause");
		myUnpauseMenuItem = new JMenuItem("Unpause");
		myStopMenuItem = new JMenuItem("Stop");
		
		myLoadEnvironmentMenuItem.addMouseListener(myLoadEnvironmentButtonListener);
		myCreateEnvironmentMenuItem.addMouseListener(myCreateEnvironmentButtonListener);
		
		myCreateNodeMenuItem.addMouseListener(myCreateNodeButtonListener);
		myRemoveNodeMenuItem.addMouseListener(myRemoveNodeButtonListener);
		myViewNodeMenuItem.addMouseListener(myViewNodeButtonListener);
		
		myStartMenuItem.addMouseListener(myStartButtonListener);
		myPauseMenuItem.addMouseListener(myPauseButtonListener);
		myUnpauseMenuItem.addMouseListener(myUnpauseButtonListener);
		myStopMenuItem.addMouseListener(myStopButtonListener);
				
		myEnvironmentMenu.add(myCreateEnvironmentMenuItem);
		myEnvironmentMenu.add(myLoadEnvironmentMenuItem);
		
		myNodeMenu.add(myCreateNodeMenuItem);
		myNodeMenu.add(myRemoveNodeMenuItem);
		myNodeMenu.add(myViewNodeMenuItem);
		
		myMenuBar.add(myEnvironmentMenu);
		myMenuBar.add(myNodeMenu);
		myMenuBar.add(myStartMenuItem);
		myMenuBar.add(myPauseMenuItem);
		myMenuBar.add(myUnpauseMenuItem);
		myMenuBar.add(myStopMenuItem);
	}
	
	private void associateListeners() 
	{
		String error;
		Class<? extends SimulationController> controllerClass;
		Method createEnvironmentMethod,
			   saveEnvironmentConditionMethod,
			   loadEnvironmentMethod,
			   loadEnvironmentConditionMethod,
			   createNodeMethod,
			   createNodeConditionMethod,
			   removeNodeMethod,
			   removeNodeConditionMethod,
			   viewNodeMethod,
			   viewNodeConditionMethod,
			   stopMethod,
			   pauseMethod, 
			   pauseNodeMethod,
			   unpauseMethod, 
			   unpauseNodeMethod,
			   startMethod;
		controllerClass = myController.getClass();
		
		createEnvironmentMethod = null;
		saveEnvironmentConditionMethod = null;
		loadEnvironmentMethod = null;
		loadEnvironmentConditionMethod = null;
		createNodeMethod = null;
		createNodeConditionMethod = null;
		removeNodeMethod = null;
		removeNodeConditionMethod = null;
		viewNodeMethod = null;
		viewNodeConditionMethod = null;
		stopMethod = null;
		pauseMethod = null;
		pauseNodeMethod = null;
		unpauseMethod = null;
		unpauseNodeMethod = null;
		startMethod = null;
		
		try 
		{
			createEnvironmentMethod = controllerClass.getMethod("createEnvironment", (Class<?>[])null);
			saveEnvironmentConditionMethod = controllerClass.getMethod("saveCondition", (Class<?>[])null);
			loadEnvironmentMethod = controllerClass.getMethod("loadEnvironment", (Class<?>[])null);
			loadEnvironmentConditionMethod = controllerClass.getMethod("loadCondition", (Class<?>[])null);
			createNodeMethod = controllerClass.getMethod("createNode", (Class<?>[])null);
			createNodeConditionMethod = controllerClass.getMethod("createNodeCondition", (Class<?>[])null);
			removeNodeMethod = controllerClass.getMethod("removeNode", (Class<?>[])null);
			removeNodeConditionMethod = controllerClass.getMethod("removeNodeCondition", (Class<?>[])null);
			viewNodeMethod = controllerClass.getMethod("viewNode", (Class<?>[])null);
			viewNodeConditionMethod = controllerClass.getMethod("viewNodeCondition", (Class<?>[])null);
			stopMethod = controllerClass.getMethod("stop", (Class<?>[])null);
			pauseMethod = controllerClass.getMethod("pause", (Class<?>[])null);
			pauseNodeMethod = controllerClass.getMethod("pauseNode", (Class<?>[])null);
			unpauseMethod = controllerClass.getMethod("unpause", (Class<?>[])null);
			unpauseNodeMethod = controllerClass.getMethod("unpauseNode", (Class<?>[])null);
			startMethod = controllerClass.getMethod("start", (Class<?>[])null);
		}
		catch (NoSuchMethodException e)
		{
			System.out.println(e);	
		}
		catch (SecurityException e)
		{
			System.out.println(e);	
		}
		
		myCreateEnvironmentButtonListener = new ButtonListener(myController, createEnvironmentMethod, null);
		mySaveEnvironmentConditionButtonListener = new ButtonListener(myController, saveEnvironmentConditionMethod, null);
		myLoadEnvironmentButtonListener = new ButtonListener(myController, loadEnvironmentMethod, null);
		myLoadEnvironmentConditionButtonListener = new ButtonListener(myController, loadEnvironmentConditionMethod, null);
		myCreateNodeButtonListener = new ButtonListener(myController, createNodeMethod, null);
		myCreateNodeConditionButtonListener = new ButtonListener(myController, createNodeConditionMethod, null);
		myRemoveNodeButtonListener = new ButtonListener(myController, removeNodeMethod, null);
		myRemoveNodeConditionButtonListener = new ButtonListener(myController, removeNodeConditionMethod, null);
		myViewNodeButtonListener = new ButtonListener(myController, viewNodeMethod, null);
		myViewNodeConditionButtonListener = new ButtonListener(myController, viewNodeConditionMethod, null);
		myStopButtonListener = new ButtonListener(myController, stopMethod, null);
		myPauseButtonListener = new ButtonListener(myController, pauseMethod, null);
		myPauseNodeButtonListener = new ButtonListener(myController, pauseNodeMethod, null);
		myUnpauseButtonListener = new ButtonListener(myController, unpauseMethod, null);
		myUnpauseNodeButtonListener = new ButtonListener(myController, unpauseNodeMethod, null);
		myStartButtonListener = new ButtonListener(myController, startMethod, null);
	}
	
	public void displayNodeInformationPanel()
	{
		this.hideSidePanels();
		myNodeInformationPanel.setVisible(true);
	}
	
	public void displayCreateEnvironmentPanel()
	{
		this.hideSidePanels();
		myCreateEnvironmentPanel.setVisible(true);
	}
		
	public String getCreateEnvironmentNameTextField()
	{
		return myCreateEnvironmentNameTextField.getText();
	}
	
	public String getCreateEnvironmentNodeCountTextField()
	{
		return myCreateEnvironmentNodeCountTextField.getText();
	}
	
	public String getCreateEnvironmentHubCountTextField()
	{
		return myCreateEnvironmentHubCountTextField.getText();
	}
	
	public String getCreateEnvironmentWidthTextField()
	{
		return myCreateEnvironmentWidthTextField.getText();
	}
	
	public String getCreateEnvironmentHeightTextField()
	{
		return myCreateEnvironmentHeightTextField.getText();
	}
	
	public void setEnvironmentImage(BufferedImage bi)
	{
		myEnvironmentView.setEnvironmentImage(bi);
		myEnvironmentView.repaint();
		System.out.println("here");
	}
	
	public int getSelectedCondition()
	{
		return mySavedEnvironmentSettingsJList.getSelectedIndex();
	}
	
	public int getCreateNodeXTextField()
	{
		return Integer.parseInt(myCreateNodeXTextField.getText());
	}
	
	public int getCreateNodeYTextField()
	{
		return Integer.parseInt(myCreateNodeYTextField.getText());
	}
	
	public boolean getCreateNodeHubCheckBox()
	{
		return myCreateNodeHubCheckBox.isSelected();
	}
	
	public int getSelectedRemoveNodeCondition()
	{
		return myRemoveNodeJList.getSelectedIndex();
	}

	public int getSelectedViewNodeCondition()
	{
		return myViewNodeJList.getSelectedIndex();
	}
	
	public void updateNodeInformation(NodePosition n)
	{
		myNodeIDLabel.setText(n.getNode().getID()[0] + " " + n.getNode().getID()[1] + " " + n.getNode().getID()[2]);
		ArrayList<Packet> inbox = n.getNode().getCurrentInbox();
		String str = "";
		for (int i = 0; i < inbox.size(); i ++)
		{
			str += inbox.get(i).getMessage() + " ID " +inbox.get(i).getHeader().getID() + "\n";
		}
		myNodeMessagesTextArea.setText(str);
		
		ArrayList<Neighbor> neighbors = n.getNode().getNeighbors();
		str = "";
		for (int i = 0; i < neighbors.size(); i ++)
		{
			str += neighbors.get(i).getStepsFromHub() + " ID " + neighbors.get(i).getID()[0] + " " + neighbors.get(i).getID()[1] + " " + neighbors.get(i).getID()[2] + "\n";
		}
		myNodeNeighborsTextArea.setText(str);
		
		myNodeTotalMessagesReceivedLabel.setText(n.getNode().getMessageCount() + "");
		myNodeNeighborLabel.setText(n.getNode().getNeighbors().size() + "");
		myNodeStepsFromHub.setText(n.getNode().getStepsFromHub() + "");
		str = "";
		if (n.getNode().getReceivingList().size() > 0)
		{
			int[] id = n.getNode().getReceivingList().get(0).getID(); 
			str = id[0] + " " + id[1] + " " + id[2];
		}
		myNodeCurrentReceiverLabel.setText(str);
		
		myEnvironmentView.setNodeX(n.getXPosition());
		myEnvironmentView.setNodeY(n.getYPosition());
		myEnvironmentView.repaint();
	}
	
	/*
	 *	HIDE AND DISPLAY 
	 *
	 */
	
	public void hidePause()
	{
		myPauseMenuItem.setVisible(false);
	}
	
	public void hideUnpause()
	{
		myUnpauseMenuItem.setVisible(false);
	}
	
	public void hideStart()
	{
		myStartMenuItem.setVisible(false);
		this.displayPause();
		this.displayStop();
	}
	
	public void hideStop()
	{
		myStopMenuItem.setVisible(false);
		this.displayStart();
	}
	
	public void hideNodeMenu()
	{
		myNodeMenu.setVisible(false);
	}
	
	public void displayPause()
	{
		myPauseMenuItem.setVisible(true);
	}
	
	public void displayUnpause()
	{
		myUnpauseMenuItem.setVisible(true);
	}
	
	public void displayStart()
	{
		myStartMenuItem.setVisible(true);
	}

	public void displayStop()
	{
		myStopMenuItem.setVisible(true);
	}
	
	public void displayNodeMenu()
	{
		myNodeMenu.setVisible(true);
	}
	
	public void displayCreateNodePanel()
	{
		myCreateNodePanel.setVisible(true);
	}
	
	public void displayRemoveNodePanel()
	{
		myRemoveNodePanel.setVisible(true);
	}
	
	public void displayAllSystemConditions(ArrayList<ArrayList> list)
	{
		this.hideSidePanels();
		mySavedEnvironmentSettingsDefaultList.removeAllElements();
		for (int i = 0; i < list.size(); i ++)
		{
			if (list.get(i).size() > 5)
			{
				mySavedEnvironmentSettingsDefaultList.addElement(i + " | " +list.get(i).get(0) + " | " + list.get(i).get(5));
			}
		}

		myLoadEnvironmentPanel.setVisible(true);
	}
	
	public void displayAllRemoveNode(ArrayList<NodePosition> list)
	{
		this.hideSidePanels();
		myRemoveNodeDefaultList.removeAllElements();
		for (int i = 0; i < list.size(); i ++)
		{
			myRemoveNodeDefaultList.addElement(i + " | " + list.get(i).getXPosition() + " | " + list.get(i).getYPosition());
		}

		myRemoveNodePanel.setVisible(true);
	}
	
	public void displayAllViewNode(ArrayList<NodePosition> list)
	{
		this.hideSidePanels();
		myViewNodeDefaultList.removeAllElements();
		for (int i = 0; i < list.size(); i ++)
		{
			myViewNodeDefaultList.addElement(i + " | " + list.get(i).getXPosition() + " | " + list.get(i).getYPosition());
		}

		myViewNodePanel.setVisible(true);
	}
	
	/*
	 * Disable and enable
	 */
	public void disableNodePause()
	{
		myPauseNodeButton.setEnabled(false);
		myUnpauseNodeButton.setEnabled(true);
	}

	/*
	 * Disable and enable
	 */
	public void enableNodePause()
	{
		myPauseNodeButton.setEnabled(true);
		myUnpauseNodeButton.setEnabled(false);
	}
}
