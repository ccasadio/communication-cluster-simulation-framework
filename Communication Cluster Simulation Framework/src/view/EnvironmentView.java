package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;

import model.NodeController;
import environment.Environment;

public class EnvironmentView  extends JComponent
{
	private int myWidth,
				myHeight;
	private int myNodeX,
				myNodeY;
	private boolean mySet;
	private BufferedImage myEnvironmentImage;

	public EnvironmentView(int width, int height)
	{
		this.mySet = false;
		this.myWidth = width;
		this.myHeight = height;
		this.myNodeX = Environment.IGNORE;
		this.myNodeY = Environment.IGNORE;
		this.myEnvironmentImage = null;
		this.setPreferredSize(new Dimension(width, height));
	}
	
    public void paintComponent(Graphics g) 
    {
        super.paintComponent(g);
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, myWidth, myHeight);

        paintGraphicalObjects(g);
    }

    public void paintGraphicalObjects(Graphics g)
    {
    	if (mySet)
    	{
        	g.drawImage(myEnvironmentImage, 0, 0, View.HEIGHT, View.HEIGHT, this);	
    	}
    	if (this.myNodeX != Environment.IGNORE && this.myNodeY != Environment.IGNORE)
    	{

    		double ratio = (double) View.HEIGHT / (double) myEnvironmentImage.getWidth();
    		g.setColor(Color.RED);
    		double s = ((double) NodeController.SIGNAL_STRENGTH) * ratio;
    		int x = (int) Math.floor(((double) myNodeX * ratio) - (s / 1.5));
    		int y = (int) Math.floor(((double) myNodeY * ratio) - (s / 1.5));
    		g.drawOval(x, y, (int) Math.floor(s * 1.5), (int) Math.floor(s * 1.5));
    	}
    }

    public void setEnvironmentImage(BufferedImage bi)
    {
    	myEnvironmentImage = bi;
    	mySet = true;
    }
    
    public void setNodeX(int x)
    {
    	this.myNodeX = x;
    }
    
    public void setNodeY(int y)
    {
    	this.myNodeY = y;
    }

    public int getWidth()
    {
    	return this.myWidth;
    }

    public int getHeight()
    {
    	return this.myHeight;
    }
}
