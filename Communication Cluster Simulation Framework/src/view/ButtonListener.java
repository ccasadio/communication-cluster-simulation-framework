package view;

import java.awt.event.*;
import java.lang.reflect.*;

public class ButtonListener extends MouseAdapter
{
	private Object myController;
	private Method myMethod;
	private Object[] myArgs;
	
	public ButtonListener(Object controller, Method method, Object[] args)
	{
		myController = controller;
		myMethod = method;
		myArgs = args;
	}
	
	public void mouseReleased(MouseEvent e)
	{
		Method method;
		Object controller;
		Object[] arguments;
		
		method = this.getMethod();
		controller = this.getController();
		arguments = this.getArguments();
		
		try
		{
			method.invoke(controller, arguments);
		}
		catch(InvocationTargetException error)
		{
			System.out.println("Error Invocation");
			System.out.println(error.getMessage());
		}
		catch(IllegalArgumentException error)
		{
			System.out.println("Error argument");
			System.out.println(error.getMessage());

		}
		catch(IllegalAccessException error)
		{
			System.out.println("Error access");
			System.out.println(error.getMessage());

		}
	}
	
	public void setArguments(Object[] args)
	{
		myArgs = args;
	}
	
	public void setController(Object controller)
	{
		myController = controller;
	}
	
	public void setMethod(Method method)
	{
		myMethod = method;
	}
	
	public Method getMethod()
	{
		return myMethod;
	}
	
	public Object[] getArguments()
	{
		return myArgs;
	}
	
	public Object getController()
	{
		return myController;
	}
}
