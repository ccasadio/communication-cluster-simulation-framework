package environment;

/**
 * The Environment class is there to represent the entire map of 
 * where the simulation is to occur. This is represented by a Color
 * Map object which stores an id (Red Green and Blue) at a position.
 * The environment has then a list of the nodes and a list of the 
 * TransmissionImpedingObjects.
 * 
 * @author Christian Casadio, Christian Micklisch
 */

import java.util.ArrayList;

import communication.Parcel;
import node.*;

public class Environment 
{
	public static final int IGNORE = -1;
	public static final int DEFAULT = 0;
	public static final int HIGH = 255;
	public static final int MID = 125;
	public static final int[] DEFAULT_RGB = {DEFAULT, DEFAULT, DEFAULT, DEFAULT};
	public static final int[] NODE_RGB = {HIGH, IGNORE, IGNORE, HIGH};
	public static final int[] HUB_RGB = {MID, HIGH, IGNORE, HIGH};
	public static final int[] BIDIRECTIONAL_NODE_RGB = {HIGH + IGNORE, IGNORE, IGNORE, HIGH};
	private ColorMap myColorMap;
	private ArrayList<NodePosition> myNodeList;
	private ArrayList<NodePosition> myTransMissionImpedingObjectList;
	
	public Environment(ColorMap map)
	{
		myNodeList = new ArrayList<NodePosition>();
		myTransMissionImpedingObjectList = new ArrayList<NodePosition>();
		myColorMap = map;
		this.findNodes();
	}
	
	public Environment(int nodeCount, int hubCount, int width, int height)
	{
		myNodeList = new ArrayList<NodePosition>();
		myTransMissionImpedingObjectList = new ArrayList<NodePosition>();
		this.setupNodes(this.createNodes(nodeCount), width, height);
		this.setupHubs(hubCount);
		myColorMap = new ColorMap(myNodeList, myTransMissionImpedingObjectList, width, height);
	}
	
	/**
	 * The setupNodes method goes and gives each node a position by attempting to place
	 * them equally apart in a square like pattern. For the rest of the nodes that could
	 * not be placed in the square like pattern they are placed randomly in the environment
	 * where a node has not yet been placed.
	 * 
	 * @param nodeList
	 * @param width
	 * @param height
	 */
	public void setupNodes(Node[] nodeList, int width, int height)
	{
		int p = 0;
		int l = ((int)Math.floor(Math.sqrt(nodeList.length) * (width/height)));
		int r = ((int)Math.floor(Math.sqrt(nodeList.length) * (height/width)));
		
		for(int i = 0; i < l; i ++)
		{
			for(int j = 0; j < r; j++)
			{
				NodePosition position = new NodePosition(nodeList[p], (i * (width/l)), (j * (height/r)));
				myNodeList.add(position);
				p ++;
			}
		}
		
		int x = 0;
		int y = 0;
		
		while(p < nodeList.length)
		{
			x = (int) (Math.floor(Math.random() * width));
			y = (int) (Math.floor(Math.random() * height));
			if (!checkPositionTaken(x, y))
			{
				NodePosition position = new NodePosition(nodeList[p], x, y);
				myNodeList.add(position);
				p++;	
			}
		}
	}
	
	/**
	 * The setupHubs method goes and changes one of the nodes to a hub not only through a 
	 * change in the class but also a change in the 
	 */
	public void setupHubs(int hubCount)
	{
		int k = 0;
		while (k < hubCount)
		{
			int i = (int) Math.floor(Math.random() * myNodeList.size());
			if (!myNodeList.get(i).getNode().getClass().equals("node.Hub"))
			{
				Node n = myNodeList.get(i).getNode();
				int[] id = n.getID();
				for (int j = 0; j < HUB_RGB.length; j ++)
				{
					if (HUB_RGB[j] != IGNORE)
					{
						id[j] = HUB_RGB[j];
					}
				}
				Hub h = new Hub(id);
				myNodeList.get(i).setNode((Node) h);
				k ++;
			}		
		}
	}
	
	/**
	 * The checkPositionTaken method goes and checks to see if the exact position has been
	 * taken. If the position has been taken then it returns true, otherwise the position
	 * has not been taken yet.
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean checkPositionTaken(int x, int y)
	{
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			if (myNodeList.get(i).getXPosition() == x && myNodeList.get(i).getYPosition() == y)
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * The createNode method creates a node in the environment by first checking if the 
	 * position has already been taken, if it has not then it creates a node ID based off
	 * of the current size of the Node List. It then creates a new node, gives it the 
	 * requested position, and then adds it to the color map.
	 * @param x
	 * @param y
	 */
	public void createNode(int x, int y)
	{
		if (!checkPositionTaken(x,y))
		{
			int [] id = {NODE_RGB[0], NODE_RGB[1], NODE_RGB[2], NODE_RGB[3]};
			id[1] = myNodeList.size() - 1;
			id[2] = (int) Math.abs(((myNodeList.size() - 1 * 254) % 255));
			myNodeList.add(new NodePosition(new Node(id), x, y));
			// update color map
			myColorMap.addNodeList(myNodeList);
		}
	}
	
	/**
	 * The createHub method creates a hub in the environment by first checking if the 
	 * position has already been taken, if it has not then it creates a hub ID based off
	 * of the current size of the Node List. It then creates a new Hub, gives it the 
	 * requested position, and then adds it to the color map.
	 * @param x
	 * @param y
	 */
	public void createHub(int x, int y)
	{
		if (!checkPositionTaken(x,y))
		{
			int [] id = {HUB_RGB[0], HUB_RGB[1], HUB_RGB[2], HUB_RGB[3]};
			id[1] = myNodeList.size() - 1;
			id[2] = (int) Math.abs(((myNodeList.size() - 1 * 254) % 255));
			myNodeList.add(new NodePosition(new Hub(id), x, y));
			// update color map
			myColorMap.addNodeList(myNodeList);
		}
	}
	
	/**
	 * The removeNode method goes and removes the node provided by the index given,
	 * it then clears the entire color list and then adds the entire node list back
	 * on to the color list, thereby creating a new environment with the previous 
	 * nodes.
	 * 
	 * @param index
	 */
	public void removeNode(int index)
	{
		myNodeList.remove(index);
		myColorMap.clearColorList();
		myColorMap.addNodeList(myNodeList);
	}
	
	/*
	 * change documentation
	 */
	/**
	 * The findNodes method goes and finds all of the Nodes in the colorMap object
	 * and then returns them as ObjectPosition ArrayList. This information is then
	 * stored in the nodeList.
	 */
	private void findNodes()
	{
		myNodeList = myColorMap.findObjectsByID(NODE_RGB);
		ArrayList<NodePosition> hubList = myColorMap.findObjectsByID(HUB_RGB);
		for (int i = 0; i < hubList.size(); i ++)
		{
			myNodeList.add(hubList.get(i));
		}
	}
	
	/**
	 * The getNodePosition method goes and gets the NodePostion based off of the  given 
	 * position in the parameters.
	 * 
	 * @param x
	 * @param y
	 */
	public NodePosition getNodePosition(int x, int y)
	{
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			if (myNodeList.get(i).getXPosition() == x && myNodeList.get(i).getYPosition() == y)
			{
				return myNodeList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * The createNodes method goes and creates the nodes, the nodes
	 * only need an ID assocaited with them, so for the Node only the
	 * blue and green values are changed, as denoted by the 1, and 2
	 * array positions.
	 *  
	 * @param numNodes
	 * @return
	 */
	private Node[] createNodes(int numNodes)
	{
		Node[] nodeList = new Node[numNodes];
		for(int i = 0; i < numNodes; i ++)
		{
			int [] id = {NODE_RGB[0], NODE_RGB[1], NODE_RGB[2], NODE_RGB[3]};
			id[1] = i;
			id[2] = (int) Math.abs(((i * 254) % 255));
			nodeList[i] = new Node(id);
		}
		return nodeList;
	}
	
	public void setupTransmissionImpedingObjects(TransmissionImpedingObject[] transImpObj)
	{
		
	}
	
	public void loadState()
	{
		
	}
	
	public void saveState()
	{
		
	}
	
	/**
	 * The findRecipients method goes through to find all of the nodes that should
	 * receive the package based off of which nodes are in the area to actually receive
	 * the message.
	 * 
	 * @param parcel
	 * @return
	 */
	public ArrayList<Parcel> findRecipients(Parcel parcel)
	{
		int[] id = parcel.getNode().getID();
		int[] center = this.getNodePos(id);
		int radius = parcel.getMaxDistance();
		int type[] = {NODE_RGB[0], HUB_RGB[0]};
		ArrayList<Parcel> parcels = new ArrayList<Parcel>();
		ArrayList<int[]> recipients = myColorMap.findObjectsInRadius(center, radius, type);
//		System.out.println("here now " + recipients.size());
		
		for(int i = 0; i < recipients.size(); i++)
		{
			// here we go and find out whom this can be sent to.
			parcel.setNode(getNodePointer(recipients.get(i)));
			parcels.add(parcel.clone());
		}
		
		return parcels;
	}
	
	public int findDistance(int[] object1ID, int[] object2ID)
	{
		//use raycasting method in colormap
		return myColorMap.calculateDistance(object1ID, object2ID);
	}
	
	/**
	 * The getNodePos method goes and gets the position of the node via using the id.
	 * This is done via going through all of the nodeList and checking to see if the
	 * id in the list matches the id in the parameter. If this is done then the X and 
	 * Y coordinate is stored in a single dimensional array.
	 * 
	 * @param nodeID	The ID of the node (Color ID).
	 * @return
	 */
	private int[] getNodePos(int[] nodeID)
	{
		for(int i = 0; i < myNodeList.size(); i ++)
		{
			NodePosition op = myNodeList.get(i);
			int[] id = ((Node) op.getNode()).getID();
			if(myColorMap.compareID(id, nodeID))
			{
				int[] pos = {op.getXPosition(), op.getYPosition()};
				return pos;
			}
		}
		return null;
	}
	
	/**
	 * The getNodePointer method goes and gets the memory address of the Node (pointer)
	 * simply by going through the node list and comparing the id's to find the instance
	 * of the nodeID in the nodeList. It then returns the memory address of that node 
	 * instance.
	 * 
	 * @param nodeID
	 * @return
	 */
	private Node getNodePointer(int[] nodeID)
	{
		//find node's memory address using it's ID
		//should probably modify myNodeList so that it also contains mem addresses
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			int[] id = ((Node) myNodeList.get(i).getNode()).getID();
			if (myColorMap.compareID(id, nodeID))
			{ 
				return (Node) myNodeList.get(i).getNode().clone();
			}
		}
		return null;
	}
	
	private boolean compareIDs(int[] idOne, int[] idTwo)
	{
		return idOne[0] == idTwo[0] && idOne[1] == idTwo[1] && idOne[2] == idTwo[2];
	}
	
	public ArrayList<NodePosition> getTransmissionImpedingObjects()
	{
		return myTransMissionImpedingObjectList;
	}
	
	public ArrayList<NodePosition> getNodeList()
	{
		return myNodeList;
	}
	
	public ColorMap getColorMap()
	{
		return myColorMap;
	}
}
