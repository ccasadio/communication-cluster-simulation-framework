package environment;

/**
 * The ColorMap class is there to create and hold the objects
 * that have been created in the environment. All of these
 * object hold a certain ID with them (a specific RGB value), and
 * these ID's define the object to be either a Node or a 
 * TransmissionImpedingObject. 
 * 
 * @author Christian Casadio, Christian Micklisch
 */

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import node.Hub;
import node.Node;

public class ColorMap 
{
	private int[][][] myColorList;
	private Random myRandom;
	
	/**
	 * The ColorMap constructor a width, and then a height. The Color list matrix is first 
	 * initialized with the proper size. The matrix is then initialized to have each point 
	 * contain the default ID set by the Environment class. 
	 * 
	 * @param x
	 * @param y
	 */
	public ColorMap(int width, int height)
	{
		myColorList = new int[width][height][4];
        for(int i = 0; i < width; i ++)
        {
        	for(int j = 0; j < height; j++)
        	{
        		myColorList[i][j] = Environment.DEFAULT_RGB;
        	}
        }
	}
	
	/**
	 * The ColorMap constructor takes in a list of nodes, a list of transmission impeding
	 * objects, a width, and then a height. The Color list matrix is first initialized with
	 * the proper size. The matrix is then initialized to have each point contain the default
	 * ID set by the Environment class. This default ID states that nothing exists in this
	 * location. Then both lists are sent to their respective methods to add them properly 
	 * to the matrix.
	 * 
	 * @param nodeList
	 * @param TransMissImpObjList
	 * @param width
	 * @param height
	 */
	public ColorMap(ArrayList<NodePosition> nodeList, ArrayList<NodePosition> TransMissImpObjList, int width, int height)
	{
		myColorList = new int[width][height][3];
        for(int i = 0; i < width; i ++)
        {
        	for(int j = 0; j < height; j++)
        	{
        		myColorList[i][j] = Environment.DEFAULT_RGB;
        	}
        }
		this.addNodeList(nodeList);
		this.addTransmissionImpedingObjectList(TransMissImpObjList);
	}
	
	/** 
	 * The addNodeList method goes and adds all of the  object stored in the 
	 * incoming ArrayList. This array list is of type integer array of size 5. 
	 * The first 2 array points are the position of the Node object. The last 
	 * 3 points are the ID.
	 * 
	 * @param transmissionImpedingObjectList
	 */
	public void addNodeList(ArrayList<NodePosition> nodeList)
	{
		for(int i = 0; i < nodeList.size(); i ++)
		{
			int[] id = ((Node) nodeList.get(i).getNode()).getID();
			this.addObject(nodeList.get(i).getXPosition(), nodeList.get(i).getYPosition(), id);
		}
	}
	
	/**
	 * The addTransmissionImpedingObjectList method goes and adds all of the 
	 * object stored in the incoming ArrayList. This array list is of type
	 * integer array of size 5. The first 2 array points are the position of 
	 * the Transmission Impeding Object. The last 3 points are the ID.
	 * 
	 * currently unused and so not important
	 * 
	 * @param transmissionImpedingObjectList
	 */
	public void addTransmissionImpedingObjectList(ArrayList<NodePosition> transmissionImpedingObjectList)
	{
		for(int i = 0; i < transmissionImpedingObjectList.size(); i ++)
		{
//			int[] id = ((TransmissionImpedingObject) transmissionImpedingObjectList.get(i).getNode()).getID();
//			this.addObject(transmissionImpedingObjectList.get(i).getXPosition(), transmissionImpedingObjectList.get(i).getYPosition(), id);
		}
	}
	
	/**
	 * The findObjectsByID method goes and checks all of the integer arrays in
	 * the colorList and checks if their id is similar to that of the id passed
	 * in the parameter.
	 * 
	 * @param id
	 * @return
	 */
	public ArrayList<NodePosition> findObjectsByID(int[] id)
	{
		ArrayList<NodePosition> list = new ArrayList<NodePosition>();
		for (int i = 0; i < myColorList.length; i ++)
		{
			for (int j = 0; j < myColorList[0].length; j ++)
			{
				if (compareID(id, myColorList[i][j]) && compareID(id, Environment.NODE_RGB))
				{
					Node node = new Node(myColorList[i][j]);
					list.add(new NodePosition(node, i, j));
				}
				else if (compareID(id, myColorList[i][j]) && compareID(id, Environment.HUB_RGB))
				{
					Hub node = new Hub(myColorList[i][j]);
					list.add(new NodePosition(node, i, j));
				}
					
			}
		}
		return list;	
	}
	
	/**
	 * The compareID method goes and checks both integer array to 
	 * see if they are exaclty the same or not, if the first array has 
	 * a value of IGNORE in one of its locations then that location is 
	 * ignored.
	 * 
	 * @param id
	 * @param comparisonID
	 * @return
	 */
	public boolean compareID(int[] id, int[] comparisonID)
	{
		
		for(int k = 0; k < myColorList[0][0].length; k ++)
		{
			if(id[k] != Environment.IGNORE)
			{
				if(comparisonID[k] != id[k])
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * The addObject method sets a int[3] at the proper x and y position on
	 * the ColorMap matrix.
	 * 
	 * @param xPos
	 * @param yPos
	 * @param id
	 */
	public void addObject(int xPos, int yPos, int[] id)
	{
		myColorList[xPos][yPos] = id;
	}
	
	public int[] raycasting(int xPos, int yPos)
	{
		return null;
		
	}
	
	public int calculateDistance(int[] from, int[] to)
	{
		return (int) Math.floor(Math.hypot(to[1] - from[1], to[0] - from[0]));
	}
	
	/**
	 *	The findObjectsInRadius method goes and finds all of the objects in the radius
	 *	that have a specific type. This is done by going through the entire image and
	 *	and drawing a circle though that entire image only to find the object that are of
	 *	a specific type by the id that they have.
	 *	
	 *	First the method goes and gets the top left and bottom right coordinates of the circle.
	 *	If the topLeft points are less then 0 then the top left is zero. If the bottom right
	 *	is greater than the length of the color list then the image is length of the color list
	 *	(which is the length of the image).
	 *	
	 *	The method then goes through each pixel on the image starting from the top left to bottom
	 *	right x position, and then the y position. Its then calculated if the object is in the radius
	 *	of the specified circle only then to be compared if the object has the correct type.
	 *
	 *	@param center	The center of the circle stored in a integer array with the x and y.
	 *	@param radius	The radius of the circle.
	 *	@param type		The type of object that are to be searched.
	 */
	public ArrayList<int[]> findObjectsInRadius(int[] center, int radius, int[] types)
	{
		ArrayList<int[]> list = new ArrayList<int[]>();
		
		int[] currentPos = new int[2];
		int[] topLeft = {(int) (center[0] - Math.floor(radius/2)), (int) (center[1] - Math.floor(radius/2))};
		if(topLeft[0] < 0)
		{
			topLeft[0] = 0;
		}
		
		if(topLeft[1] < 0)
		{
			topLeft[1] = 0;
		}
		
		int[] bottomRight = {(int) (center[0] + Math.ceil(radius/2)), (int) (center[1] + Math.ceil(radius/2))};
		if(bottomRight[0] > myColorList.length)
		{
			bottomRight[0] = myColorList.length;
		}
		
		if(bottomRight[1] > myColorList[0].length)
		{
			bottomRight[1] = myColorList[0].length;
		}
		
		for(int i = topLeft[0]; i < bottomRight[0]; i++)
		{
			currentPos[0] = i;
			for(int j = topLeft[1]; j < bottomRight[1]; j++)
			{
				currentPos[1] = j;
				if(calculateDistance(center, currentPos) <= radius)
				{
					for (int k = 0; k < types.length; k ++)
					{
						if(myColorList[i][j][0] == types[k])
						{
							list.add(myColorList[i][j]);
						}	
					}
				}
			}
		}
		
		return list;
	}
	
	/**
	 * The clearColorList method goes and clears the color list by setting it to its
	 * default color.
	 */
	public void clearColorList()
	{
		for(int i = 0; i < myColorList.length; i++)
		{
			for(int j = 0; j < myColorList[i].length; j++)
			{
        		myColorList[i][j] = Environment.DEFAULT_RGB;
			}
		}
	}
	
	public int[][][] getColorList()
	{
		return myColorList;
	}
	
	/**
	 * The createRandomColorMap method goes and gives the matrix point a random
	 * RGB value. This random integer value is between 0 and 254.
	 */
	public void createRandomColorMap()
	{
        myRandom = new Random();
        for(int i = 0; i < myColorList.length; i++) 
        {
            for(int j = 0; j < myColorList[0].length; j++) 
            {
                myColorList[i][j][0] = Math.abs((25 * myRandom.nextInt()) % 254);
                myColorList[i][j][1] = Math.abs((25 * myRandom.nextInt()) % 254);
                myColorList[i][j][2] = Math.abs((55 * myRandom.nextInt()) % 254);
            }
        }
	}
	
	/**
	 * The getColorMapInBufferedImage method goes and converts the ColorMap into
	 * a BufferedImage object. This is done by creating a BufferedImage that has
	 * a height and width equal to that of the ColorMap object; the type of the
	 * Image is on an RGB scale as the values stored in the ColorMap are of RGB
	 * values. The image is then drawn rectangle by rectangle as a pixel as the 
	 * color value of the RGB stored in the Color Matrix. It then returns the 
	 * BufferedImage.
	 * 
	 * @return
	 */
	public BufferedImage getColorMapInBufferedImage()
	{
        BufferedImage img = new BufferedImage(myColorList.length, myColorList[0].length, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = (Graphics2D)img.getGraphics();
        for(int i = 0; i < myColorList.length; i++) 
        {
            for(int j = 0; j < myColorList[0].length; j++) 
            {
                g.setColor(new Color(myColorList[i][j][0], myColorList[i][j][1], myColorList[i][j][2], myColorList[i][j][3]));
                g.fillRect(i, j, 1, 1);
            }
        }
		return img;
	}
	
	public int getWidth()
	{
		return myColorList.length;
	}
	
	public int getHeight()
	{
		return myColorList[0].length;
	}
	
	public void print()
	{
		for(int i = 0; i < myColorList.length; i ++)
		{
			for(int j = 0; j < myColorList[0].length; j ++)
			{
				System.out.print(myColorList[i][j][0] + "-" + myColorList[i][j][1] + "-" + myColorList[i][j][2] + "/");
			}
			System.out.println();
		}
	}
}
