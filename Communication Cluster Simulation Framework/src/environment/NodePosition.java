package environment;
import node.Node;

public class NodePosition 
{
	private Node myNode;
	private int myXPosition;
	private int myYPosition;
	
	public NodePosition(Node node, int x, int y)
	{
		myNode = node;
		myXPosition = x;
		myYPosition = y;
	}
	
	public Node getNode()
	{
		return myNode;
	}
	
	public int getXPosition()
	{
		return myXPosition;
	}
	
	public int getYPosition()
	{
		return myYPosition;
	}
	
	public void setNode(Node n)
	{
		myNode = n;
	}
	
	public String toString()
	{
		String s = new String();
		s = "My X Position :" + myXPosition + " My Y Position :" + myYPosition;
		return s;
	}
}
