package node;

/**
 * The Hub class is there to act as a simplification in regards to the reactions
 * of the messages passed as it acts as a base station or cluster head. The Hub 
 * class is supposed to be the main receiver when it comes to the communication
 * of all nodes. The Hub is just an extension of the Node class.
 * 
 * @author Christian Casadio, Christian Micklisch, Katie Porterfield
 */

import java.util.ArrayList;

import model.NodeController;
import communication.Packet;
import environment.Environment;

public class Hub extends Node 
{
	protected ArrayList<Packet> myHubInbox;
	
	public Hub(int[] id)
	{
		super(id);
		myHubInbox = new ArrayList<Packet>();
		myStepsFromHub = 0;
	}
	
	/**
	 * The run method for the hub goes and runs after waiting for an alloted time
	 * and then takes the necessary defined actions via the hubReactions method and
	 * increments in current interval.
	 */
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		long tempTime = System.currentTimeMillis();

		while(myShouldRun)
		{
			this.pause(myRunPauseTime);
			if (!myPauseRun)
			{
				printResponsebox();
				this.hubReactions();
				myRunInterval ++;
			}
		}

		printAllPacketsReceived();
	}
	
	/**
	 * The hubReactions method is a bit simplified in regards to the nodeReactions
	 * where the hub first checks all of the packet that it received and then goes 
	 * into its set two phaes which are relative to the current interval that the
	 * hub is in:
	 * 
	 * NOTIFY_NEIGHBOR_INTERVAL:
	 * 		The notify neighbor interval just allows the hub to tell all of the surrounding
	 * 		nodes that the hub is the best node to send messages to.
	 * SEND_MESSAGES_INTERVAL:
	 * 		The send messages interval allows the hub to send all of the messages that
	 * 		it created in regards to the messages that it received. The messages that the
	 * 		hub usually sends are just message responses telling the previous nodes that 
	 * 		the message was received.
	 */
	public void hubReactions()
	{
		Packet packet;
		try 
		{
			while(!myInbox.isEmpty())
			{
				packet = myInbox.dequeue();
				checkPacket(packet);
			}
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.NOTIFY_NEIGHBOR_INTERVAL)
		{
	        this.notifyNeighbors();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.SEND_MESSAGES_INTERVAL)
		{
			this.sendMessages();
		}
	}
	
	/**
	 * The checkMaskPacket method goes and checks to see if the packet has the receiver of the
	 * mask id in it, if it is then there are several reactions that it must have to that packet.
	 * 
	 * If the receiver of the packet is the hubs ID then the hub checks to see if its a PASS_MESSAGE,
	 * meaning that its a message that contains information regarding a original message that a 
	 * node created. It then adds this message to the inbox of the hub and then responds to that 
	 * message.
	 * 
	 * @param packet
	 */
	protected void checkPacket(Packet packet)
	{
		if (checkPacketRecipientForID(packet, myID))
		{
			myMessageCount ++;
			String firstPart = (packet.getMessage().split(NodeController.NOTIFY_DELIMITER, 0))[0];
			if (firstPart.equals(NodeController.PASS_MESSAGE))
			{
//				System.out.println(myMessageCount);
				myHubInbox.add(packet);
//				System.out.println("I got " + packet.getMessage() + "\t" + packet.getHeader().getID());
//				System.out.println("I am " + myID[0] + "\t" + myID[1] + "\t" + myID[2] + "\t" + " I got "
//						+ packet.getHeader().getSender()[0] + "\t" + packet.getHeader().getSender()[1] + "\t" + packet.getHeader().getSender()[2]);
				createMessageResponse(packet);
			}
		}
	}
	
	public ArrayList<Packet> getCurrentInbox()
	{
		return myHubInbox;
	}
	
	protected void addToSendbox(Packet p)
	{
		mySendbox.add(p);
	}
	
	/**
	 * The getAllReceivedID method goes and gets all of the IDs in the PASS_MESSAGE from
	 * all of the messages that are in the hub current inbox. It does this by first getting
	 * the ID of the original sender in the pass message, then it compares that ID to all
	 * of the other ID's that were currently created for keeping track of all of the received
	 * nodes. If the ID is not in the current list of IDs then it is added to that list of IDs
	 * until all of the messages in the hub's inbox have been compared.
	 * 
	 * @return
	 */
	public ArrayList<int[]> getAllReceivedID()
	{
		ArrayList<int[]> list = new ArrayList<int[]>();
		for (int i = 0; i < myHubInbox.size(); i ++)
		{
			String[] parts = myHubInbox.get(i).getMessage().split(NodeController.NOTIFY_DELIMITER);
			int[] id = new int[3];
			for (int k = 0; k < parts.length; k ++)
			{
				String[] pieces = parts[k].split(NodeController.NOTIFY_INFO_DELIMITER);
				if (pieces[0].equals(NodeController.ORIGINAL_ID))
				{
					String[] original = pieces[1].split(NodeController.ID_DELIMITER);
					id[0] = Integer.parseInt(original[0]);
					id[1] = Integer.parseInt(original[1]);
					id[2] = Integer.parseInt(original[2]);
				}
			}
			boolean occured = false;
			for (int j = 0; j < list.size(); j++)
			{
				if (checkIDSimilar(id, list.get(j)) && id[0] != 0)
				{
					occured = true;
					break;
				}
			}
			if (!occured)
			{
				list.add(id);
			}
		}
		return list;
	}
	
	public void printAllPacketsReceived()
	{
		ArrayList<int[]> idList = getAllReceivedID();
		for (int i = 0; i < idList.size(); i ++)
		{
			System.out.println(idList.get(i)[0] + " " + idList.get(i)[1] + " " + idList.get(i)[2]);
		}
	}
}
