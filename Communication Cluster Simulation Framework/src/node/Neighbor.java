package node;

/**
 * The Neighbor class is there to act as a simplification of the node class to 
 * allow the current node to keep track of all of the surrounding neighbors and
 * the information regarding what the steps are from the hub, what the last interval
 * was that a message was received from the neighbor, what the current power is
 * of the neighbor, and the ID of the neighbor. 
 * 
 * @author Christian Casadio, Christian Micklisch, Katie Porterfield
 */

import environment.Environment;
import model.NodeController;

public class Neighbor 
{
	private int myCurrentPower,
				myStepsFromHub,
				myLastInterval;
	private int[] myID;
	private double mySignalStrength;
	
	public Neighbor(String message, int[] id, int interval, double signalStrength)
	{
		myCurrentPower = Environment.IGNORE;
		myStepsFromHub = Environment.IGNORE;
		myLastInterval = 0;
		mySignalStrength = signalStrength;
		myID = id;
		setup(message, interval);
	}
	
	/**
	 * The setup method goes and sets the values for the Neighbor class from the 
	 * messages that the node has received, the node also sends in its current 
	 * interval stating the last known interval that a message was received via
	 * that specific neighbor.
	 * 
	 * The message is then taken apart to extract the steps it is away from the hub
	 * and the current power that the neighbor has. The message that usually contains
	 * this information is the Notify neighbors message from that specific interval.
	 * 
	 * @param message
	 * @param interval
	 */
	public void setup(String message, int interval)
	{
		String[] parts = message.split(NodeController.NOTIFY_DELIMITER);
		for (int i = 0; i < parts.length; i ++)
		{
			if (!parts[i].equals(NodeController.NOTIFY_ADD_NEIGHBORS))
			{
				String[] pieces = parts[i].split(NodeController.NOTIFY_INFO_DELIMITER);
				if (pieces[0].equals(NodeController.NOTIFY_STEPS_FROM_HUB))
				{
					myStepsFromHub = Integer.parseInt(pieces[1]);
				}
				else if (pieces[0].equals(NodeController.NOTIFY_CURRENT_POWER))
				{
					myCurrentPower = Integer.parseInt(pieces[1]);
				}
			}
		}
		myLastInterval = interval;
	}
	
	public int getCurrentPower()
	{
		return myCurrentPower;
	}
	
	public int getStepsFromHub()
	{
		return myStepsFromHub;
	}
	
	public int getLastInterval()
	{
		return myLastInterval;
	}
	
	public void setStepsFromHub(int steps)
	{
		myStepsFromHub = steps;
	}
	
	public int[] getID()
	{
		return myID;
	}
}
