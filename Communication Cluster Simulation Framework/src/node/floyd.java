package node;

import environment.Environment;
import model.NodeController;

public class floyd
{
		public floyd()
		{


		}
		
		private int[][] flyodWarshall(int[][] path, int[][] d, int[][] p)
		{
 			for (int i = 0; i < path.length; i++)
  			{
   				for (int j = 0; j < path.length; j++)
   				{
      				    p[i][j] = 0;
          
        				d[i][j] = path[i][j];
    			 }
  			}
  
  			for (int k = 0; k < path.length; k ++)
  			{
    			for (int i = 0; i < path.length; i ++)
   				{
     				for (int j = 0; j < path.length; j ++)
     				{
        				if (d[i][k]+ d[k][j] < d[i][j])
        				{
          					p[i][j] = k;
          					d[i][j] = d[i][k] + d[k][j];
        				}
      				}
   				}
  			}	
  			return p;
		}

		//sets up path costs 
		public int[][] findShortestPath(Node[] heads)
		{
  			int[][] pathCost = new int[heads.length][heads.length];
  			int[][] d = new int[heads.length][heads.length];
  			int[][] p = new int[heads.length][heads.length];
  
  			for (int i = 0; i < heads.length; i ++)
  			{
   				 // check if the for loop
  				//places neighbors cost in array
   				 for (int j = 0; j < heads.length; j ++)
    			{
    			   pathCost[i][j] = (int)(heads.get(j).getStepsFromNeighborsHub().get(i));
    			}
  			}
  
  			return flyodWarshall(pathCost, d, p);
		}

		public Vector shortestPath(int[][] path, int q, int r)
		{
 			if (path[q][r] != 0)
 			{
   				shortestPath(path, path[q][r], r);
    			shortestPathArray.add(path[q][r]);
    			shortestPath(path,path[q][r], r); 
  			}
  			else
  			{
    			 shortestPathArray.add(r);
  			}
 			
 			return shortestPathArray; 
		}
}