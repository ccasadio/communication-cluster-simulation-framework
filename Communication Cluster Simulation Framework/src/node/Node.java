package node;

/**
 * The Node class is to act as a blueprint for the Bidirectional, 
 * UnidirectionalReceive, and UniditectionalTransmit node which
 * extend the node class. The node class contains a thread, has
 * a stopping and pausing method, includes a power source, an ID,
 * a list of its surrounding neighbors, and its own inbox.
 * 
 * @author Christian Casadio, Christian Micklisch, Katie Porterfield
 */

import java.util.ArrayList;

import model.NodeController;
import communication.Communication;
import communication.Packet;
import environment.Environment;
import powerSupply.PowerSupplyController;
import sun.misc.Queue;

public class Node implements Runnable
{
	protected Thread myThread;
	protected boolean myShouldRun;
	protected boolean myPauseRun;
	protected PowerSupplyController mySources;
	protected Queue<Packet> myInbox;
	protected ArrayList<Packet> mySendbox;
	protected ArrayList<Packet> myResponsebox;
	protected int[] myID;
	protected ArrayList<Neighbor> myNeighbors;
	protected ArrayList<Neighbor> myReceivingList;
	protected double myImportanceLevel;
	protected int myRunTime,
				myRunPauseTime,
				myRunInterval,
				myStepsFromHub,
				myMockStepsFromHub,
				myMessageCount;
	
	public Node(int[] ID)
	{
		myShouldRun = true;
		myPauseRun = false;
		myRunTime = NodeController.NODE_RUN_TIME;
		myRunPauseTime = NodeController.NODE_PAUSE_TIME;
		myID = ID;
		myInbox = new Queue<Packet>();
		mySendbox = new ArrayList<Packet>(200);
		myResponsebox = new ArrayList<Packet>(200);
		myNeighbors = new ArrayList<Neighbor>(200);
		myReceivingList = new ArrayList<Neighbor>(200);
		myRunInterval = 0;
		myMessageCount = 0;
		myMockStepsFromHub = 0;
		myImportanceLevel = 0.0;
		myStepsFromHub = Environment.IGNORE;
	}
	
	public void start() 
	{
		myThread = new Thread (this);
        myThread.start();
	}

	/**
	 * Every 5 runs it asks whom the neighbors are
	 */
	public void run() 
	{
		long startTime = System.currentTimeMillis();
		long tempTime = System.currentTimeMillis();
        
		while(myShouldRun)
		{
			this.pause(myRunPauseTime);
			if (!myPauseRun)
			{
				System.out.println("My Node is running " + myID[0] + myID[1] + myID[2]);
				this.nodeReactions();
				myRunInterval ++;
			}
		}
	}
	
	public void pause(int time)
	{
		try 
		{
			Thread.sleep(time);
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
	}
	
	public void pause()
	{
		myPauseRun = true;
	}
	
	public void unpause()
	{
		myPauseRun = false;
	}
	
	public void stop()
	{
		myShouldRun = false;
	}
	
	/**
	 * The nodeReactions method goes and decides what needs to currently be done, in this
	 * case the node first checks all of protected the packets that it has in its inbox. After that 
	 * is done it check checks which interval its in and if it will either notify its 
	 * neighbors, check if certain neighbors still exist, create a message to send to its 
	 * best neighbor, send all of the messages that are in its sendbox, or check to see 
	 * if the message has been responded to in an ample amount of time.
	 */
	public void nodeReactions()
	{
		Packet packet;
		try 
		{
			while(!myInbox.isEmpty())
			{
				packet = myInbox.dequeue();
				checkPacket(packet);
			}
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.NOTIFY_NEIGHBOR_INTERVAL)
		{
	        this.notifyNeighbors();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.NOTIFY_EXIST_INTERVAL)
		{
			this.checkNeighbor();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.CREATE_MESSAGE_INTERVAL)
		{
			this.createMessage();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.UPDATE_IMPORTANCE_LEVEL)
		{
			this.updateImportanceLevel();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.SEND_MESSAGES_INTERVAL)
		{
			this.sendMessages();
		}
		else if (myRunInterval % NodeController.NOTIFY_INTERVAL == NodeController.CHECK_RESPONSE_INTERVAL)
		{
			this.checkResponse();
		}
	}
	
	/**
	 * The checkResponse method goes and checks to see if the message has been responded to in an
	 * ample amount of time. If it has not then the message is added to the sendbox again. 
	 */
	protected void checkResponse()
	{
		for (int i = 0; i < myResponsebox.size(); i ++)
		{
			if (System.currentTimeMillis() - myResponsebox.get(i).getHeader().getCreatedMillis() > 
					NodeController.MAX_RESPONSE_WAIT_TIME_MILLIS)
			{
				mySendbox.add(myResponsebox.get(i).clone());
			}
		}
	}
	
	/**
	 * The createMessage method creates a message that contains part of its own memory address
	 * and then creates a pass message from it which contains the original sender of the message,
	 * the original message, and the amount of steps that message has taken.
	 */
	protected void createMessage()
	{
		String info = "" + this;
		info = info.split("@")[1] + System.nanoTime();
		String message = createPassMessage(myID, info, 0, null);
		addToSendbox(message);
	}
	
	/**
	 * The sendMessages method goes and sends all of the messages in the sendbox, and adds the
	 * messages on to the response box to state that the messages need a response.
	 */
	public void sendMessages()
	{
		while (mySendbox.size() > 0)
		{
			this.sendPacket(mySendbox.get(0), this.calculateSignalStrength(myImportanceLevel));
			int s =  mySendbox.get(0).getHeader().getID();
			String[] parts = mySendbox.get(0).getMessage().split(NodeController.NOTIFY_DELIMITER);
			mySendbox.remove(0);
		}
	}
	
	/**
	 * The notifyNeighbors method goes and sends a message to anyone nearby. This
	 * message contains the statement of it being a hello to everyone in its area,
	 * how much power it has and how far away it is from the hub.
	 */
	protected void notifyNeighbors()
	{
		int steps = myStepsFromHub;
//		System.out.println("Steps " + myStepsFromHub);
		// change here if necessary
//		if (myStepsFromHub > NodeController.MOCK_HUB_STEPS)
//		{
//			System.out.println("sent it");
//			steps = 0;
//		}
//		if (myReceivingList.size() >= 2
//				&& myReceivingList.get(0).getStepsFromHub() != Environment.IGNORE
//				&& myReceivingList.get(1).getStepsFromHub() == Environment.IGNORE)
//		{
//			System.out.println("sent it");
			
//			steps = 0;
//		}
		
		String message = NodeController.NOTIFY_ADD_NEIGHBORS + NodeController.NOTIFY_DELIMITER
							+ NodeController.NOTIFY_STEPS_FROM_HUB + NodeController.NOTIFY_INFO_DELIMITER
							+ steps + NodeController.NOTIFY_DELIMITER;
		// add in power consumption as well
		this.sendPacket(this.createPacket(NodeController.MASK_ID, message), calculateSignalStrength(1));
	}
	
	/**
	 * The checkNeighbor method goes and checks to see that the last time the neighbors 
	 * have been updated occurs below a certain maximum. If it does occur above a certain 
	 * maximum then the neighbor will be removed from the neighbor list and the receiving list.
	 */
	protected void checkNeighbor()
	{
		for (int i = 0; i < myNeighbors.size(); i ++)
		{
			if (myRunInterval - myNeighbors.get(i).getLastInterval() > NodeController.MAX_EXIST_INTERVAL)
			{
				int receivingIndex = getNeighborIndex(myNeighbors.get(i).getID(), myReceivingList);
				myNeighbors.get(i).setStepsFromHub(Environment.IGNORE);
				if (receivingIndex != Environment.IGNORE)
				{
					myReceivingList.remove(receivingIndex);
				}
				i --;
			}
		}
	}
	
	/**
	 * The checkMaskPacket method goes and checks to see if the packet has the receiver of the
	 * mask id in it, if it is then there are several reactions that it must have to that packet.
	 * 
	 * @param packet
	 */
	protected void checkPacket(Packet packet)
	{
		if (checkPacketRecipientForID(packet, NodeController.MASK_ID))
		{
			String firstPart = (packet.getMessage().split(NodeController.NOTIFY_DELIMITER, 0))[0];
			if (firstPart.equals(NodeController.NOTIFY_ADD_NEIGHBORS))
			{
				addNeighborResponse(packet);
			}
		}
		if (checkPacketRecipientForID(packet, myID))
		{
			myMessageCount ++;
			String firstPart = (packet.getMessage().split(NodeController.NOTIFY_DELIMITER, 0))[0];
			if (firstPart.equals(NodeController.PASS_MESSAGE))
			{
				passMessageResponse(packet);
//				createMessageResponse(packet);
			}
			else if (firstPart.equals(NodeController.RESPONSE_MESSAGE))
			{
				removeMessageResponse(packet);
			}
		}
	}
	
	/**
	 * The updateImportanceLevel method goes and decides what the necessary level
	 * of transmission is needed depending on the surrounding neighbors and the 
	 * values of their current power values, the steps from the hub, and the signal
	 * strength.  
	 */
	public void updateImportanceLevel()
	{
		myImportanceLevel = 1.0;
	}
	
	/**
	 * The createMessageResponse method goes and creates a response for the sender
	 * to be notified that the message was received by creating a packet that 
	 * includes the header id of the message received.
	 * 
	 * @param packet
	 */
	protected void createMessageResponse(Packet packet)
	{
		String message = "";
		int headerID = packet.getHeader().getID();
		message = NodeController.RESPONSE_MESSAGE + NodeController.NOTIFY_DELIMITER
					+ NodeController.HEADER_ID + NodeController.NOTIFY_INFO_DELIMITER
					+ headerID;
		
		Packet p = this.createPacket(packet.getHeader().getSender(), message);
		addToSendbox(p);
	}
	
	/**
	 * The removeMessageResponse method goes and removes the message from the response box
	 * to state that the message has been properly received.
	 * 
	 * @param packet
	 */
	protected void removeMessageResponse(Packet packet)
	{
		String[] parts = packet.getMessage().split(NodeController.NOTIFY_DELIMITER);
		for (int i = 0; i < parts.length; i ++)
		{
			String[] pieces = parts[i].split(NodeController.NOTIFY_INFO_DELIMITER);
			if (pieces[0].equals(NodeController.HEADER_ID))
			{
				removeFromResponsebox(Integer.parseInt(pieces[1]));
			}
		}
	}
	
	/**
	 * The removeFromResponsebox method goes and removes the message from the 
	 * response box via the specified headerID in the parameters.
	 * 
	 * @param headerID
	 */
	protected void removeFromResponsebox(int headerID)
	{		
		for (int i = 0; i < myResponsebox.size(); i ++)
		{
			if (myResponsebox.get(i).getHeader().getID() == headerID)
			{ 
				myResponsebox.remove(i);
				i --;
			}
		}
	}
	
	/**
	 * The addNeighborResponse method goes and takes in the information from the message
	 * to then decide if it wants to add another neighbor or update an already defined
	 * neighbor. 
	 * 
	 * @param packet
	 */
	protected void addNeighborResponse(Packet packet)
	{
		if (!checkIDSimilar(myID, packet.getHeader().getSender()))
		{
			if (!alreadyHaveNeighbor(packet.getHeader().getSender()))
			{
				Neighbor n = new Neighbor(packet.getMessage(), packet.getHeader().getSender(), myRunInterval, packet.getSignalStrength());
				myNeighbors.add(n);
			}
			else
			{
				updateNeighbor(packet.getMessage(), packet.getHeader().getSender());
			}
			updateReceivingList();
		}
	}
	
	/**
	 * The passMessageResponse method goes and creates a new message from the message
	 * received in the packet and sends it to the next node.
	 * 
	 * The message keeps track of the original ID sending the information, the total
	 * amount of steps it taken to get to this node, and the original message being sent.
	 * This information is then compiled into a new message that includes all of the
	 * previously mentioned information. 
	 * 
	 * @param packet
	 */
	protected void passMessageResponse(Packet packet)
	{
		int steps = 0;
		int[] originalID = new int[3];
		String originalMessage = "";
		String[] parts = packet.getMessage().split(NodeController.NOTIFY_DELIMITER);
		for (int i = 0; i < parts.length; i ++)
		{
			String[] pieces = parts[i].split(NodeController.NOTIFY_INFO_DELIMITER);
			if (pieces[0].equals(NodeController.MESSAGE_STEPS))
			{
				steps = Integer.parseInt(pieces[1]);
			}
			else if (pieces[0].equals(NodeController.ORIGINAL_ID))
			{
				String[] idParts = pieces[1].split(NodeController.ID_DELIMITER); 
				originalID[0] = Integer.parseInt(idParts[0]); 
				originalID[1] = Integer.parseInt(idParts[1]); 
				originalID[2] = Integer.parseInt(idParts[2]);
			}
			else if (pieces[0].equals(NodeController.ORIGINAL_MESSAGE))
			{
				originalMessage = pieces[1];
			}
		}
		steps ++;
		String message = createPassMessage(originalID, originalMessage, steps, packet);
		addToSendbox(message);
	}
	
	/**
	 * The createPassMessage method goes and creates a pass message containing the 
	 * specified id, message, and steps it is in transmission. This is all done using
	 * delimiters.
	 * 
	 * If a packet is sent into the method then it checks to see if there is any
	 * information regarding the packet already being sent to another node it then adds its
	 * ID at the end to state that is was the last one to send the message at the time. This
	 * allows for the tracking of information in regards to whom sent it and in which order.
	 * If nobody has sent the message yet then append the current nodes ID to the message.
	 * 
	 * @param id
	 * @param message
	 * @param steps
	 * @return
	 */
	protected String createPassMessage(int[] id, String message, int steps, Packet p)
	{
		String append = "";
		if (p != null)
		{
			String[] parts = p.getMessage().split(NodeController.NOTIFY_DELIMITER);
			for (int i = 0; i < parts.length; i ++)
			{
				String[] pieces = parts[i].split(NodeController.NOTIFY_INFO_DELIMITER);
				if (pieces[0].equals(NodeController.APPEND_ID))
				{
					append += parts[i] + NodeController.NOTIFY_INFO_DELIMITER 
								+ myID[0] + NodeController.ID_DELIMITER
								+ myID[1] + NodeController.ID_DELIMITER + myID[2];
				}
			}
			if (append.equals(""))
			{
				append += NodeController.APPEND_ID + NodeController.NOTIFY_INFO_DELIMITER 
						+ myID[0] + NodeController.ID_DELIMITER
						+ myID[1] + NodeController.ID_DELIMITER + myID[2];
			}
		}
		
		return NodeController.PASS_MESSAGE + NodeController.NOTIFY_DELIMITER
				+ NodeController.MESSAGE_STEPS + NodeController.NOTIFY_INFO_DELIMITER
				+ steps + NodeController.NOTIFY_DELIMITER + NodeController.ORIGINAL_ID
				+ NodeController.NOTIFY_INFO_DELIMITER + id[0] + NodeController.ID_DELIMITER
				+ id[1] + NodeController.ID_DELIMITER + id[2]
				+ NodeController.NOTIFY_DELIMITER + NodeController.ORIGINAL_MESSAGE
				+ NodeController.NOTIFY_INFO_DELIMITER + message + NodeController.NOTIFY_DELIMITER
				+ append;
	}
	
	public void addToInbox(Packet pk)
	{
		myInbox.enqueue(pk);
	}
	
	/**
	 * The addToSendbox method goes and adds the message to the sendbox by creating
	 * a packet with the best receiver on  the receiving list.
	 * 
	 * @param message
	 */
	protected void addToSendbox(String message)
	{
		if (myReceivingList.size() > 0)
		{
			Packet p = this.createPacket(myReceivingList.get(0).getID(), message);
			mySendbox.add(p);
		}
	}
	
	/**
	 * The addToSendbox method adds a packet to a sendbox only when there is a
	 * neighbor to send it to.
	 * 
	 * @param p
	 */
	protected void addToSendbox(Packet p)
	{
		if (myReceivingList.size() > 0)
		{
			mySendbox.add(p);
		}
	}
	
	/**
	 * The addToResponse method checks to see if the message has already been added to
	 * the response box, if it hasn't then it will add the packet.
	 * 
	 * @param p
	 */
	protected void addToResponsebox(Packet p)
	{
		boolean occured = false;
		
		for (int i = 0; i < myResponsebox.size(); i ++)
		{
			if (myResponsebox.get(i).getHeader().getID() == p.getHeader().getID())
			{
				occured = true;
				break;
			}
		}
		if (!occured)
		{
			myResponsebox.add(p);
		}
	}
	
	protected boolean checkMessageError(Packet pk)
	{
		return true;
	}
	
	/**
	 * The checkPacketRecipientForID method goes and checks to compare the id inside
	 * the packet to the id passed in the parameters. This method only compares the 
	 * two parts of an ID only if the parts of the ID's are not to be ignored.
	 * 
	 * @param pk
	 * @param id
	 * @return
	 */
	protected boolean checkPacketRecipientForID(Packet pk, int[] id)
	{
		int[] packetID = pk.getHeader().getReceiver();
		for (int i = 0; i < packetID.length && i < id.length; i ++)
		{
			if (packetID[i] != id[i] && (id[i] != Environment.IGNORE || packetID[i] != Environment.IGNORE))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @return
	 */
	protected int calculateSignalStrength(double importance)
	{
		return ((int) Math.floor(NodeController.SIGNAL_STRENGTH * importance));
	}
	
	protected boolean checkMessageLength(Packet pk)
	{
		return (pk.getMessage().length() == pk.getHeader().getMessageLength());
	}
	
	protected int[] decideMessageAction()
	{
		return null;
	}
	
	protected Packet createPacket(int[] recieverID, String data)
	{
		return new Packet(data, myID, recieverID);
	}
	
	protected void sendPacket(Packet pk, int sigStr)
	{
		Communication.sendMessage(pk, sigStr, this);
	}
	
	public int[] getID()
	{
		return myID;
	}
	
	public Queue<Packet> getInbox()
	{
		return myInbox;
	}
	
	public ArrayList<Neighbor> getNeighbors()
	{
		return myNeighbors;
	}
	
	public ArrayList<Neighbor> getReceivingList()
	{
		return myReceivingList;
	}
	
	public boolean running()
	{
		return myShouldRun;
	}
	
	public Node clone()
	{
		return this;
	}
	
	public int getMessageCount()
	{
		return myMessageCount;
	}
	
	public int getStepsFromHub()
	{
		return myStepsFromHub;
	}
	
	public boolean getPaused()
	{
		return myPauseRun;
	}
	
	/**
	 * The checkIDSimilar method goes and checks both integer array to 
	 * see if they are similar or not, if the first array has a value of
	 * IGNORE in one of its locations then that location is ignored.
	 * 
	 * @param id
	 * @param comparisonID
	 * @return
	 */
	protected boolean checkIDSimilar(int[] id, int[] comparisonID)
	{		
		for(int k = 0; k < id.length; k ++)
		{
			if(id[k] != Environment.IGNORE)
			{
				if(comparisonID[k] != id[k])
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	/**
	 * The alreadyHaveNeighbor method goes and checks to see if the neighbor already
	 * exists on the neighbor list.
	 * 
	 * @param newID
	 * @return
	 */
	protected boolean alreadyHaveNeighbor(int[] newID)
	{
		for (int i = 0; i < myNeighbors.size(); i ++)
		{
			if(checkIDSimilar(myNeighbors.get(i).getID(), newID))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * The updateNeighbor method updates a Neighbor with a specific id by matching it and
	 * then calling its setup method to find the best id.
	 * 
	 * @param message
	 * @param id
	 */
	protected void updateNeighbor(String message, int[] id)
	{
		for (int i = 0; i < myNeighbors.size(); i ++)
		{
			if(checkIDSimilar(myNeighbors.get(i).getID(), id))
			{
				myNeighbors.get(i).setup(message, myRunInterval);
				break;
			}
		}
	}
	
	/**
	 * The updateReceivingList method goes and updates its ReceivingList from the 
	 * best node to send to, to the worst node to send to. This is done by getting
	 * a list of all of the neighbors and ordering them from greatest to least by 
	 * the amount of steps it is from the hub. The less amount of steps it takes the
	 * more important it is and so the earlier its added on to the list.
	 */
	protected void updateReceivingList()
	{		
		ArrayList<Neighbor> tempArray = (ArrayList<Neighbor>) myNeighbors.clone();
		myReceivingList.clear();
		
		while (tempArray.size() > 0)
		{
			int index = 0;
			for (int i = 0; i < tempArray.size(); i ++)
			{
				if (myNeighbors.get(i).getID()[0] == Environment.HUB_RGB[0])
				{
					index = i;
					break;
				}
				else if (tempArray.get(index).getStepsFromHub() > tempArray.get(i).getStepsFromHub()
							&& tempArray.get(i).getStepsFromHub() != Environment.IGNORE)
				{
					index = i;
				}
			}
			if (tempArray.get(index).getStepsFromHub() != Environment.IGNORE)
			{
				myReceivingList.add(tempArray.get(index));
			}
			tempArray.remove(index);
		}

		if (myReceivingList.size() > 0)
		{
			int amount =  myReceivingList.get(0).getStepsFromHub();
			if (amount != Environment.IGNORE)
			{
				myStepsFromHub = amount + 1;	
//				System.out.println("My current amount is " + myStepsFromHub + " ID is " + myID[0] + " " + myID[1] + " " + myID[2]);
			}
		}
//		this.printReceivingList();
	} 
	
	/**
	 * The getNeighborStepsFromHub method goes and gets the steps the from the neighbor
	 * with a specific id.
	 * 
	 * @param id
	 * @return
	 */
	protected int getNeighborStepsFromHub(int[] id)
	{
		for (int i = 0; i < myNeighbors.size(); i ++)
		{
			if(checkIDSimilar(myNeighbors.get(i).getID(), id))
			{
				return myNeighbors.get(i).getStepsFromHub();
			}
		}
		return Environment.IGNORE;
	}

	/**
	 * The getNeighborIndex method goes and gets the index of the neighbor from
	 * the provided list. If the neighbor does not exist then it returns a Ignore
	 * from the Environment.
	 * 
	 * @param id
	 * @param list
	 * @return
	 */
	protected int getNeighborIndex(int[] id, ArrayList<Neighbor> list)
	{
		for (int i = 0; i < list.size(); i ++)
		{
			if(checkIDSimilar(list.get(i).getID(), id))
			{
				return i;
			}
		}
		return Environment.IGNORE;
	}
	
	public ArrayList<Packet> getCurrentInbox()
	{
		Queue<Packet> copyInbox = new Queue<Packet>();
		ArrayList<Packet> copyList = new ArrayList<Packet>();
		
		Packet p = null;
		
		while (!myInbox.isEmpty())
		{
			try 
			{
				p = myInbox.dequeue();
				copyList.add(p.clone());
				copyInbox.enqueue(p.clone());
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		myInbox = copyInbox;
		
		return copyList;
	}
	
	protected void printNeighbors()
	{
		for (int i = 0; i < myNeighbors.size(); i ++)
		{
			System.out.print("Hi! I'm " + myID[1] + " " + myID[2] + " and my neighbor is");
			System.out.println(" " + i + " is " + myNeighbors.get(i).getID()[1] + " " + myNeighbors.get(i).getID()[2]);
		}
	}

	protected void printReceivingList()
	{
		for (int i = 0; i < myReceivingList.size(); i ++)
		{
			System.out.print("Hi! I'm " + myID[0] + " " + myID[1] + " " + myID[2] + " and my receiver is");
			System.out.println(" " + i + " is " +  myReceivingList.get(i).getID()[0] + " " + myReceivingList.get(i).getID()[1] + " " + myReceivingList.get(i).getID()[2]);
		}
	}
	
	protected void printResponsebox()
	{
		for (int i = 0; i < myResponsebox.size(); i ++)
		{
			System.out.println(this + " " + myResponsebox.size() + " " + myResponsebox.get(i).getMessage());
		}
	}
}
