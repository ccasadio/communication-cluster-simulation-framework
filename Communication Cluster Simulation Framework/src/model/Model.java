package model;

/**
 * The Model class is there to act as a holder for all information regarding the
 * entire model this includes the NodeController, and the Environment that the
 * NodeController is housed in.
 * 
 * @author Christian Casadio, Christian Micklisch
 */

import environment.ColorMap;
import environment.Environment;

public class Model 
{	
	private Environment myEnvironment;
	private NodeController myNodeController;
	private boolean myPauseRun;
	private int myMessagesToReceive;
	private int myNewCreatedNodes;
	private String myName;
	
	public Model()
	{
		myPauseRun = false;
		myNewCreatedNodes = 0;
	}
	
	/**
	 * The setupModel method goes and sets the values of the environments colorMap,
	 * the nodeControllers data generation specified by the Data Generator class, 
	 * and the messagePassingType for the MessagePassingDecisionEngine. It also sets
	 * the models total amount of messages that it is to receive.
	 * 
	 * @param colorMap
	 * @param messagePassingType
	 * @param dataGeneratorType
	 * @param messagesToReceive
	 */
	public void setupModel(ColorMap colorMap, String messagePassingType, String dataGeneratorType,
							int messagesToReceive, String name)
	{
		myEnvironment = new Environment(colorMap);
		myNodeController = new NodeController(messagePassingType, dataGeneratorType, myEnvironment);
		myMessagesToReceive = messagesToReceive;
		myName = name;
	}
	
	/**
	 * The setupModel method goes and sets the values of the environments nodeCount, 
	 * width, and height, the nodeControllers data generation specified by the Data 
	 * Generator class, and the messagePassingType for the MessagePassingDecisionEngine. 
	 * It also sets the models total amount of messages that it is to receive.
	 * 
	 * @param colorMap
	 * @param messagePassingType
	 * @param dataGeneratorType
	 * @param messagesToReceive
	 */
	public void setupModel(int nodeCount, int hubCount, int width, int height, String messagePassingType, 
							String dataGeneratorType, int messagesToReceive, String name)
	{
		myEnvironment = new Environment(nodeCount, hubCount, width, height);
		myNodeController = new NodeController(messagePassingType, dataGeneratorType, myEnvironment);
		myMessagesToReceive = messagesToReceive;
		myName = name;
	}
	
	public void createNode(int x, int y, boolean isHub)
	{
		if (!isHub)
		{
			myEnvironment.createNode(x,y);	
		}
		else
		{
			myEnvironment.createHub(x,y);
		}
		myNodeController.startNewNodes(1);
	}
	
	public void removeNode(int index)
	{
		if (index > -1 && index < myEnvironment.getNodeList().size())
		{
			myEnvironment.removeNode(index);	
		}
	}
	
	public void start()
	{
		myNodeController.start();
	}
	
	public void stop()
	{
		myNodeController.stop();
	}
	
	public int getTotalAmountOfMessagesReceived()
	{
		return myNodeController.getHubMessagesReceived();
	}
	
	public Environment getEnvironment()
	{
		return myEnvironment;
	}
	
	public NodeController getNodeController()
	{
		return myNodeController;
	}
	
	public int getMessagesToReceive()
	{
		return myMessagesToReceive;
	}
	
	public void stopNodes()
	{
		myNodeController.stop();
	}
	
	public void pauseNodes()
	{
		myNodeController.pause();
		myPauseRun = true;
	}
	
	public void unpauseNodes()
	{
		myNodeController.unpause();
		myPauseRun = false;
	}
	
	public String getName()
	{
		return myName;
	}
}
