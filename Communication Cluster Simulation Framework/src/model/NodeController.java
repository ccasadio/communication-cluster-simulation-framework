package model;

/**
 * The NodeController class exists to constantly check on the Nodes in
 * the Environment. This class starts each of the nodes, runs them,
 * constantly checks their status, and aids them in sending messages to 
 * each other.
 * 
 * @author Christian Casadio, Christian Micklisch, Katie Porterfield
 */

import java.util.ArrayList;







import communication.Communication;
import communication.Packet;
import communication.Parcel;
import node.Node;
import environment.Environment;
import environment.NodePosition;

public class NodeController implements Runnable
{
	public static final String NOTIFY_DELIMITER = ":";
	public static final String NOTIFY_INFO_DELIMITER = "!";
	public static final String ID_DELIMITER = ";";
	public static final int[] MASK_ID = {255, 255, 255};
	public static final String NOTIFY_ADD_NEIGHBORS = "I'm your Neighbor";
	public static final String NOTIFY_STEPS_FROM_HUB = "I am near hub by";
	public static final String NOTIFY_CURRENT_POWER = "I have this much power";
	public static final String PASS_MESSAGE = "Passing Message";
	public static final String ORIGINAL_ID = "Original ID";
	public static final String APPEND_ID = "Append ID";
	public static final String HEADER_ID = "Header ID";
	public static final String ORIGINAL_MESSAGE = "Original Message";
	public static final String MESSAGE_STEPS = "Steps";
	public static final String RESPONSE_MESSAGE = "Responding Message";
	public static final int SIGNAL_STRENGTH = 50;
	public static final int NOTIFY_INTERVAL = 6;
	public static final int NOTIFY_NEIGHBOR_INTERVAL = 0;
	public static final int NOTIFY_EXIST_INTERVAL = 1;
	public static final int CREATE_MESSAGE_INTERVAL = 2;
	public static final int UPDATE_IMPORTANCE_LEVEL = 3;
	public static final int SEND_MESSAGES_INTERVAL = 4;
	public static final int CHECK_RESPONSE_INTERVAL = 5;
	public static final int MAX_EXIST_INTERVAL = 30;
	public static final int MOCK_HUB_STEPS = 1;
	public static final long MAX_RESPONSE_WAIT_TIME_MILLIS = 10000;
	public static final int NODE_RUN_TIME = 70000;
	public static final int NODE_PAUSE_TIME_EXTRA = 200;
	public static final int NODE_TIME_DIFFERENCE = 20;
	public static int NODE_PAUSE_TIME = 2000; 
	
	private Environment myEnvironment;
	private ArrayList<Node> myNodeList;
	private Thread myThread;
	private String myMessagePassingType;
	private String myDataGeneratorType;
	private boolean myShouldRun;
	private boolean myPauseRun;
	private boolean myStarted;
	
	public NodeController(String messagePassingType, String dataGeneratorType, Environment env)
	{
		myShouldRun = true;
		myStarted = false;
		myPauseRun = false;
		myMessagePassingType = messagePassingType;
		myDataGeneratorType = dataGeneratorType;
		myEnvironment = env;
		myNodeList = new ArrayList<Node>();
		Communication.constructQueue();
		this.setupNodeList(myEnvironment.getNodeList());
	}
	
	public void setupNodeList(ArrayList<NodePosition> nodeList)
	{
		for(int i = 0;i < nodeList.size(); i ++)
		{
			myNodeList.add(((Node) nodeList.get(i).getNode()).clone());		
		}
	}
	
	/**
	 * The startNewNodes method goes and starts the nodes based off of the
	 * new node count given in the parameters.
	 * 
	 * @param newNodeCount
	 */
	public void startNewNodes(int newNodeCount)
	{
		this.setupNodePauseTime(myNodeList.size() + newNodeCount, NODE_TIME_DIFFERENCE, NODE_PAUSE_TIME_EXTRA);
		ArrayList<NodePosition> list = myEnvironment.getNodeList();
		
		for (int j = 0; j < newNodeCount; j ++)
		{
			myNodeList.add(((Node) list.get(j + myNodeList.size()).getNode()).clone());
		}
		
		if (myStarted)
		{
			for (int i = myNodeList.size() - newNodeCount; i < myNodeList.size(); i ++)
			{
				myNodeList.get(i).start();
			}
		}
	}
	
	/**
	 * The setupNodePauseTime method goes and sets up the node pause time to the specified 
	 * node size, this is then optimized for how the nodes interact with each other at their
	 * own specified interval.
	 * 
	 * @param nodeSize
	 * @param nodeTimeDifference
	 * @param extraTime
	 */
	public void setupNodePauseTime(double nodeSize, double nodeTimeDifference, double extraTime)
	{
		NODE_PAUSE_TIME = (int) ((nodeSize * nodeTimeDifference) + extraTime);
	}
	
	/**
	 * The start method of the NodeController is there to start all of the Nodes
	 * that the NodeController has in his list. The NodeController also needs to 
	 * start his own thread allowing him to send messages to the necessary recipients.
	 */
	public void start() 
	{
		this.setupNodePauseTime(myNodeList.size(), NODE_TIME_DIFFERENCE, NODE_PAUSE_TIME_EXTRA);
		myThread = new Thread(this);
		myThread.start();
		
		for(int i = 0; i < myNodeList.size(); i ++)
		{
			myNodeList.get(i).start();
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		myStarted = true;
	}
	
	public void pause()
	{
		myPauseRun = true;
		
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			myNodeList.get(i).pause();
		}
	}
	
	public void unpause()
	{
		myPauseRun = false;
		
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			myNodeList.get(i).unpause();
		}
	}
	
	/**
	 * The stop method of the NodeController sets the myShouldRun boolean to false,
	 * this then directly relates to the run method in which the while loop is running
	 * that constantly checks for any messages to be sent in the Communications queue.
	 */
	public void stop()
	{
		myShouldRun = false;
		for (int i = 0; i < myNodeList.size(); i ++)
		{
			myNodeList.get(i).stop();
		}
	}
	
	/**
	 * The Node Controller runs until all the required messages have been sent to the
	 * Hub. While it is running it is checking if any of the Nodes have sent a message
	 * to the Communications queue. If there is something in the communications queue 
	 * then the NodeController will call the Communications method deliverMessage to
	 * give whomever is to receive that message. This message is delivered to those
	 * whom are in range of receiving the message, which is received from the Environment.
	 */
	public void run() 
	{
		while (myShouldRun)
		{
			if (!myPauseRun)
			{
				this.checkCommunicationSendQueue();
				// get nodes in the senders area,
				// deliver messages to all the nodes in the area	
			}
		}
	}
	
	/**
	 * The getHubMessagesReceived method goes and counts up the total amount of messages 
	 * that all of the hubs have received in the entire simulation.
	 * 
	 * @return
	 */
	public int getHubMessagesReceived()
	{
		return 0;
	}
	
	public ArrayList<Node> getNodeList()
	{
		return myNodeList;
	}
	
	/**
	 * The checkCommunicationSendQueue method goes and finds all of recipients for the packet
	 * gives it an impedance value, a packet strength (which is just the max distance over 
	 * the distance of the recipient squared) and then delivers that packet to the specified 
	 * recipient
	 */
	public void checkCommunicationSendQueue()
	{
		Parcel parcel = Communication.getParcel();
		ArrayList<Parcel> myRecipients = null;
		//don't remember if things can be checked against null <- ooooooh scary! signed Fuzzy
		if(parcel != null)
		{
			myRecipients = myEnvironment.findRecipients(parcel);
			int impedanceValue;
			for(int i = 0; i < myRecipients.size(); i ++)
			{
				impedanceValue = 0;
				Packet pk = myRecipients.get(i).getPacket();
				pk.setSignalStrength( ((double) parcel.getMaxDistance()) / Math.pow(myEnvironment.findDistance(myRecipients.get(i).getNode().getID(), parcel.getNode().getID()), 2));
				Communication.deliverMessage(pk, pk.getSignalStrength(), impedanceValue, myRecipients.get(i).getNode());
			}
		}
	}
	
	public String getMessagePassingType()
	{
		return myMessagePassingType;
	}
	
	public String getDataGeneratorType()
	{
		return myDataGeneratorType;
	}
}
